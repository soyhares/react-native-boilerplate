import { StyleSheet, Platform } from 'react-native';
import { createTheming } from '@callstack/react-theme-provider';
import Palette from './palette';
import Metrics from './metrics';
import Fonts from './fonts';
import Normalize from './normalizeText';

/**
 * @var {Object} defaultTheme
 * @property {boolean} dark Se encarga de saber si es un tema oscuro.
 */
export const defaultTheme = {
	palette: Palette,
	font: Fonts,
	grid: Metrics.Grid,
	height: Metrics.Height,
	width: Metrics.Width,
	spacing: Metrics.Spacing,
	dark: false,
	roundness: 3,
	elevation: 4,
	borderBase: StyleSheet.hairlineWidth,
	boxShadow: Platform.select({
		android: { elevation: 3 },
		ios: {
			shadowColor: Palette.black,
			shadowOffset: {
				width: 0,
				height: 0.75
			},
			shadowOpacity: 0.24,
			shadowRadius: 2 // debe ser elevation - 1
		}

	})
};

export const { ThemeProvider, useTheme, withTheme } = createTheming(defaultTheme);

export { StyleSheet, Palette, Metrics, Fonts, Normalize };
