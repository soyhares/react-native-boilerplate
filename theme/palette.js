/**
 * La idea es tener aquí todos los colores con nombre,
 * como sea, y depues setear el tema en base a ellos y además,
 * concatenarlos en el objeto Palette. Los Nombres de theme.palette NO
 * se deben modificar.
 */
const namedColors = {
	white: '#FFFFFF',
	azure: '#FAFAFA',
	snow: '#ECEFF1',
	orangered: '#F44336',
	darkred: '#B00020',
	black: '#000000',
	gray: '#666666',
	dimgray: '#303030',
	silver: '#A1A1A1',
	lightgray: '#B0BEC5',
	cornflowerblue: '#3b5998',
	divider: '#E8E8E8',
	darkOpacity: '#00000088',
	dullnavy: '#20376A',
	firebrick: '#B22222',
	brightcyan: '#46B89C',
	freeze: '#3951C5'
};

/**
 * @var Palette <Object> App theme colors
 */
const Palette = {
	primary: namedColors.dullnavy,
	textPrimary: namedColors.black,
	textOnPrimary: namedColors.white,
	secondary: namedColors.white,
	accent: namedColors.brightcyan,
	background: namedColors.snow,
	surface: namedColors.azure,
	error: namedColors.darkred,
	textOnWhite: namedColors.gray,
	textOnBlack: namedColors.white,
	disabled: namedColors.black,
	placeholder: namedColors.black,
	backdrop: namedColors.black,
	notification: namedColors.black,
	header: namedColors.snow,
	footer: namedColors.dullnavy,
	divider: namedColors.divider,
	facebook: namedColors.cornflowerblue,
	...namedColors
};

export default Palette;
