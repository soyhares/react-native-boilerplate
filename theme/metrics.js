import { Dimensions, StatusBar, Platform } from 'react-native';

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');

/**
 * @function BASE_GRID Base para el espaciado del app.
 * @param {number} n Multiplier
 */
const BASE_GRID = n => n * 4;

/**
 * @var {Object} Grid
 */
const Grid = {
	h1: BASE_GRID(0.5),
	x0: 0,
	x1: BASE_GRID(1),
	x2: BASE_GRID(2),
	x3: BASE_GRID(3),
	x4: BASE_GRID(4),
	x5: BASE_GRID(5),
	x6: BASE_GRID(6),
	x8: BASE_GRID(8),
	x10: BASE_GRID(10)
};

const Spacing = {
	base: 'x5',
	scaled: 'x0'
};

const Height = {
	footer: 65,
	bottomTabs: 56,
	header: 44,
	button: 40,
	divider: 3,
	input: 56,
	wizard: 60,
	item: 50,
	badge: 20,
	avatar: 76,
	icon: 28, // 20 mockUP,
	statusBar: Platform.select({
		ios: 20, // la mínima es 20, Se debe implemetar para saber la de iphone x y xs
		android: StatusBar.currentHeight
	})
};

// statusBar
// ipad mini y ipad 1er gen 20px
// ipads y iphones pequeños 40px
// iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus, iPhone 8 Plus 54px
// iphone x 88
// iphone xs xr 132


const Width = {
	button: undefined,
	divider: 25,
	border: 1,
	badge: 20,
	avatar: 76,
	icon: 28 // 20 mockUP
	//	border: StyleSheet.hairlineWidth
};

export default {
	Grid,
	Spacing,
	Height,
	Width,
	WIDTH,
	HEIGHT
};


