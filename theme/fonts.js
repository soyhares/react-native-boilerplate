import normalize from './normalizeText';

const FONT_SIZE_BASE = 14;

/**
 * @var {object} types
 */
const types = {
	default: 'Roboto-Regular',
	black: 'Roboto-Black',
	light: 'Roboto-Light',
	medium: 'Roboto-Medium',
	condensed: 'RobotoCondensed-Regular',
	bold: 'RobotoSlab-Bold'
};

/**
 * @var {object} size
 */
const size = {
	xxxs: (FONT_SIZE_BASE - 5), // 9 // tabs label
	xxs: (FONT_SIZE_BASE - 3), // 11
	xs: (FONT_SIZE_BASE - 2), // 12
	sm: (FONT_SIZE_BASE - 1), // 13
	normal: (FONT_SIZE_BASE), // 14
	md: (FONT_SIZE_BASE + 1), // 15
	lg: (FONT_SIZE_BASE + 4), // 18
	xl: (FONT_SIZE_BASE + 6), // 20
	xxl: (FONT_SIZE_BASE + 11), // 25
	xxxl: (FONT_SIZE_BASE + 22) // 36
};

const Font = {
	types,
	size
};

export default Font;
