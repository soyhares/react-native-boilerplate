// Rojos
// lightsalmon 	FF A0 7A 	255 160 122
// salmon 	FA 80 72 	250 128 114
// darksalmon 	E9 96 7A 	233 150 122
// lightcoral 	F0 80 80 	240 128 128
// indianred 	CD 5C 5C 	205  92  92
// crimson 	DC 14 3C 	220  20  60
// firebrick 	B2 22 22 	178  34  34
// darkred 	8B 00 00 	139   0   0
// red 	FF 00 00 	255   0   0

// Orange colors
// orangered 	FF 45 00 	255  69   0
// tomato 	FF 63 47 	255  99  71
// coral 	FF 7F 50 	255 127  80
// darkorange 	FF 8C 00 	255 140   0
// orange 	FF A5 00 	255 165   0

// Purple, violet, and magenta colors
// lavender 	E6 E6 FA 	230 230 250
// thistle 	D8 BF D8 	216 191 216
// plum 	DD A0 DD 	221 160 221
// violet 	EE 82 EE 	238 130 238
// orchid 	DA 70 D6 	218 112 214
// fuchsia 	FF 00 FF 	255   0 255
// magenta 	FF 00 FF 	255   0 255
// mediumorchid 	BA 55 D3 	186  85 211
// mediumpurple 	93 70 DB 	147 112 219
// blueviolet 	8A 2B E2 	138  43 226
// darkviolet 	94 00 D3 	148   0 211
// darkorchid 	99 32 CC 	153  50 204
// darkmagenta 	8B 00 8B 	139   0 139

// purple 	80 00 80 	128   0 128
// indigo 	4B 00 82 	 75   0 130
// darkslateblue 	48 3D 8B 	 72  61 139
// slateblue 	6A 5A CD 	106  90 205
// mediumslateblue  	7B 68 EE 	123 104 238

// White colors
// white 	FF FF FF 	255 255 255
// snow 	FF FA FA 	255 250 250
// honeydew 	F0 FF F0 	240 255 240
// mintcream 	F5 FF FA 	245 255 250
// azure 	F0 FF FF 	240 255 255
// aliceblue 	F0 F8 FF 	240 248 255
// ghostwhite 	F8 F8 FF 	248 248 255
// whitesmoke 	F5 F5 F5 	245 245 245
// seashell 	FF F5 EE 	255 245 238
// beige 	F5 F5 DC 	245 245 220
// oldlace 	FD F5 E6 	253 245 230
// floralwhite 	FF FA F0 	255 250 240
// ivory 	FF FF F0 	255 255 240
// antiquewhite 	FA EB D7 	250 235 215
// linen 	FA F0 E6 	250 240 230
// lavenderblush 	FF F0 F5 	255 240 245
// mistyrose 	FF E4 E1 	255 228 225

// gray and black colors
// gainsboro 	DC DC DC 	220 220 220
// lightgray 	D3 D3 D3 	211 211 211
// silver 	C0 C0 C0 	192 192 192
// darkgray 	A9 A9 A9 	169 169 169
// gray 	80 80 80 	128 128 128
// dimgray 	69 69 69 	105 105 105
// lightslategray 	77 88 99 	119 136 153
// slategray 	70 80 90 	112 128 144
// darkslategray 	2F 4F 4F 	 47  79  79
// black 	00 00 00 	  0   0   0


// navy colors
// dullNavy 20 37 6A  32 55 106

// magenta colors
//hollywoodcersie FF 00 A2  255 0 162