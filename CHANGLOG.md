# Changelog
Todos los cambios notables a este proyecto serán documentados en este archivo.

El formato se basa en [Mantener un registro de cambios] (https://keepachangelog.com/en/1.0.0/),
y este proyecto se adhiere a [Versiones semánticas] (https://semver.org/spec/v2.0.0.html).

Consulte las etiquetas modificadas, eliminadas, agregadas y corregidas.

## [0.2.0] - Tue 16 Jul 2019 04:47:52 PM CST
### Added
- Se cambió el input y el switch para soportar formik.
- Se ha añadido un nuevo formulario.
- Se ha habilitado un formulario de pruebas en iniciar con email.
- Se agregaron las dependencias Yup, recompose, Formik y DeviceInfo.
## [0.1.0] - Sat 13 Jul 2019 02:08:05 PM CST
### Added
- Se agregó plantilla de módulo de tipo de acción.
- Se ha añadido la plantilla del módulo de acción.
- Se ha habilitado el generador de submódulos + redux.
- Se agregaron salidas para redux-actions y redux-types.

## [0.0.2] - Sat 13 Jul 2019 12:08:05 PM CST
### Changed
- El helper writeOnFile se cambió para exportar acciones de redux.
- El helper toClassName fue cambiado por el modificador de plop-properCase.

## [0.0.1] - Sat 13 Jul 2019 10:35:15 AM CST
### Added
- Seguimiento inicial de version.json.
- Este archivo CHANGELOG servirá como un ejemplo en evolución de un proyecto de código abierto estandarizado CHANGELOG.
