import { Easing, Animated } from 'react-native';

export default () => ({
	transitionSpec: {
		duration: 400,
		easing: Easing.out(Easing.poly(4)),
		timing: Animated.timing,
		useNativeDriver: true
	},
	screenInterpolator: ({
		position,
		layout: { initWidth: width },
		scene: { index: thisSceneIndex }
	}) => ({
		transform: [{
			translateX: position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [width, 0]
			})
		}]
	})
});
