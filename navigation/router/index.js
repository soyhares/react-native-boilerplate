import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { defaultTheme } from 'appTheme';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Tabs } from 'appComponents';

// import SwipeTransition from '../transition';
import * as Boot from '../../modules/boot';
import * as Auth from '../../modules/auth';
import * as AppTabs from '../../modules/Tabs';
import * as Settings from '../../modules/settings';

const defaultNavigatorConfig = (initialRouteName, transitionConfig) => ({
	initialRouteName,
	headerMode: 'none',
	transitionConfig
});

const defaultTabsConfig = {
	defaultNavigationOptions: ({
		navigation: {
			state: { routeName }
		}
	}) => ({
		tabBarIcon: Tabs.Icon(routeName),
		tabBarLabel: Tabs.Label(routeName)
	}),
	tabBarOptions: {
		inactiveBackgroundColor: defaultTheme.palette.primary,
		activeBackgroundColor: defaultTheme.palette.primary,
		tabStyle: { flexDirection: 'column' },
		style: {
			borderTopWidth: 0,
			height: defaultTheme.height.bottomTabs,
			backgroundColor: defaultTheme.palette.primary
		}
	}
};

const UserAuthStack = createStackNavigator(
	{
		_authSignIn: Auth.SignIn,
		_authSignInWithEmail: Auth.SignInWithEmail
	},
	defaultNavigatorConfig('_authSignIn')
);

const PrivateTabsNavigator = createBottomTabNavigator(
	{
		_tabHome: Boot.DevPage,
		_tabSettings: AppTabs.SettingsTab
	},
	defaultTabsConfig
);

const PublicTabsNavigator = createBottomTabNavigator(
	{
		_tabJoin: Boot.DevPage,
		_tabSettings: AppTabs.SettingsTab
	},
	defaultTabsConfig
);

const sharedScreens = {
	// ? Pantallas para ambas sesiones
	_devPage: Boot.DevPage
};

const AppStack = createStackNavigator(
	{
		...sharedScreens,
		_dashboard: PrivateTabsNavigator
	},
	defaultNavigatorConfig('_dashboard')
);

const GuestStack = createStackNavigator(
	{
		...sharedScreens,
		_dashboard: PublicTabsNavigator,
		_guestAuth: UserAuthStack
	},
	defaultNavigatorConfig('_dashboard')
);

const AppRootNavigation = createSwitchNavigator(
	{
		_splash: Boot.Splash,
		_lang: Settings.ChooseLanguage,
		_auth: UserAuthStack,
		_tips: Boot.DevPage,
		_app: AppStack,
		_guest: GuestStack
	},
	defaultNavigatorConfig('_splash')
);

export default createAppContainer(AppRootNavigation);
