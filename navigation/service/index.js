import { NavigationActions, StackActions, withNavigation } from 'react-navigation';
import Otron from 'reactotron-react-native';

let navigatorRef;

/**
 * @StackActions
 *
POP:Navigation/POP
POP_TO_TOP:Navigation/POP_TO_TOP
PUSH:Navigation/PUSH
RESET:Navigation/RESET
REPLACE:Navigation/REPLACE
COMPLETE_TRANSITION:Navigation/COMPLETE_TRANSITION
pop: pop()
popToTop: popToTop()
push: push()
reset: reset()
replace: replace()
completeTransition: completeTransition()
 */

/**
 * @NavigationActions
 *
BACK:Navigation/BACK
INIT:Navigation/INIT
NAVIGATE:Navigation/NAVIGATE
SET_PARAMS:Navigation/SET_PARAMS
back: back()
init: init()
navigate: navigate()
setParams: setParams()
 */

function setTopLevelNavigator(ref) {
	navigatorRef = ref;
}

function navigate(routeName, params) {
	navigatorRef.dispatch(
		NavigationActions.navigate({
			routeName,
			params
		})
	);
}

function replace(routeName, params) {
	if (Array.isArray(routeName) && routeName.length > 1) {
		navigatorRef.dispatch(
			StackActions.replace({
				routeName: routeName.shift(),
				action: NavigationActions.navigate({ routeName: routeName.shift(), params })
			})
		);
	} else {
		navigatorRef.dispatch(
			StackActions.replace({
				routeName,
				params
			})
		);
	}
}

const resetNavigationTo = (route, params) => {
	const action = StackActions.reset({
		index: 0,
		actions: [NavigationActions.navigate({ routeName: route, params })]
	});
	navigatorRef.dispatch(action);
};

const goBack = () => {
	navigatorRef.dispatch(NavigationActions.back());
};

const popToTop = () => navigatorRef.dispatch(StackActions.popToTop());

const logger = (prevState, nextState, action) => {
	// if (__DEV__) {
	// 	Otron.display({
	// 		name: 'NAVIGATION',
	// 		preview: `${action.type} - ${action.routeName}`,
	// 		value: {
	// 			action,
	// 			nextState,
	// 			prevState
	// 		}
	// 	});
	// }
};

export default {
	replace,
	goBack,
	withNavigation,
	popToTop,
	resetNavigationTo,
	navigate,
	setTopLevelNavigator,
	logger
};
