import R from 'ramda';
import Store from '../../redux/store';
//import sanitizeHtml from 'sanitize-html/dist/sanitize-html';

// valid value
const isValidValue = R.complement(R.either(R.isNil, R.isEmpty));

// valid number
const isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)));

// sorter
const sortingCollectionBy = (data, key, asc) => R.sort(R[asc ? 'ascend' : 'descend'](R.prop(key)), data);

// searcher
const filteringCollectionBy = (data, keySearch, entry, criteria) => {
	const insideOf = (val, entryVal) => ((R.type(criteria) === 'Function')
		? criteria(val, entryVal)
		: R.contains(entryVal.toLowerCase(), val.toLowerCase())); // terms
	const isValid = (obj, typeOf = 'Object') => (!R.isNil(obj) && R.type(obj) === typeOf); // valid data
	const finder = (item, key, val) => (isValid(item) ? insideOf(item[key], val) : false); // finder
	return R.filter(e => finder(e, keySearch, entry), data);
};

// url validation
const isValidURL = (uri) => {
	const isURL = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
	return isURL.test(uri);
}

// capitalize text
export const capEntry = txt => R.join('', [R.toUpper(R.head(txt)), R.drop(1, txt)]);

// sanitizeHtml
// export const getTextFromHtml = html => sanitizeHtml(html, { allowedTags: [], allowedAttributes: [] });


export const dispatchService = action => Store.dispatch(action);


export default {
	capEntry,
	isValidURL,
	isValidValue,
	isValidNumber,
	dispatchService,
	sortingCollectionBy,
	filteringCollectionBy
};
