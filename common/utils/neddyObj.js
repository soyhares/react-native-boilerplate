/*
	NeddyObj
		Description: Obtiene el objeto que se necesitan y como se necesita.
		author: Harley Espinoza
		updated: Sun 14 Jul 2019 07:33:10 PM CST

 	¿Como se usa NeddyObj?
		1. Pasale como 1re parametro un objeto del cual obtener los datos.
		2. Pasale como 2do parametro el objeto que necesitas y como lo necesitas.
		3. Listo!

	¿Como creo el objeto que necesito?
		1. Asigna a la key del objeto creado la etiqueta que necesitas para ver su valor.
		2. Asigna al valor, una cadena en dotCase como el path del valor en el objeto original.
		3. Listo!

	¿Como le paso mi valor de reemplazo al value?
		1. Cambia la cadena por array.
		2. Coloca en la posición 0 el path y en la posición 1 el valor de reemplazo.
		3. Listo!
		NOTA: Si solo pasas el path, el reemplazo sera undefined.

	¿Como le paso mi función modificadora al value?
		1. Cambia la cadena por array.
		2. Coloca en la posición 0 el path, en la 1 un valor de reemplazo y en la 2 la función .
		3. Listo!

	?[EJEMPLO]: https://ramdajs.com/repl/?v=0.26.1#?%2F%2A%0A%09NeddyObj%0A%09Description%3A%20Obtiene%20el%20objeto%20que%20se%20necesitan%20y%20como%20se%20necesita.%0A%09author%3A%20Harley%20Espinoza%0A%09createdAt%3A%20Sun%2014%20Jul%202019%2007%3A33%3A10%20PM%20CST%0A%2A%2F%0Aconst%20print%20%3D%20console.log%3B%0Aconst%20isArray%20%3D%20entry%20%3D%3E%20%28type%28entry%29%20%3D%3D%3D%20%27Array%27%29%3B%0Aconst%20isObject%20%3D%20entry%20%3D%3E%20%28type%28entry%29%20%3D%3D%3D%20%27Object%27%29%3B%0Aconst%20isFunction%20%3D%20entry%20%3D%3E%20%28type%28entry%29%20%3D%3D%3D%20%27Function%27%29%3B%0Aconst%20isString%20%3D%20entry%20%3D%3E%20%28type%28entry%29%20%3D%3D%3D%20%27String%27%29%3B%0Aconst%20isValidValue%20%3D%20complement%28either%28isNil%2C%20isEmpty%29%29%3B%0Aconst%20getPath%20%3D%20entry%20%3D%3E%20split%28%27.%27%2C%20entry%29%3B%0Aconst%20neddyValue%20%3D%20%28value%2C%20origin%29%20%3D%3E%20%7B%0A%09if%20%28isValidValue%28value%29%29%20%7B%0A%09%09if%20%28isArray%28value%29%29%20%7B%0A%09%09%09const%20%5BexpectedValue%2C%20replacement%2C%20enhancer%5D%20%3D%20value%3B%0A%09%09%09const%20result%20%3D%20pathOr%28replacement%2C%20getPath%28expectedValue%29%2C%20origin%29%3B%0A%09%09%09if%20%28isValidValue%28enhancer%29%20%26%26%20isFunction%28enhancer%29%29%20return%20enhancer%28result%29%3B%0A%09%09%09return%20result%3B%0A%09%09%7D%0A%09%09return%20pathOr%28undefined%2C%20getPath%28value%29%2C%20origin%29%3B%0A%09%7D%0A%09return%20undefined%3B%0A%7D%3B%0Aconst%20neddyObj%20%3D%20%28origin%2C%20expected%2C%20namespace%29%20%3D%3E%20%7B%0A%09const%20TAG%20%3D%20%28namespace%20%26%26%20isString%28namespace%29%29%20%3F%20%60NeddyObj-%24%7Bnamespace%7D%60%20%3A%20%27NeddyObj%27%3B%0A%09if%20%28isObject%28origin%29%20%26%26%20isObject%28expected%29%29%20%7B%0A%09%09if%20%28%21isEmpty%28origin%29%20%26%26%20%21isEmpty%28expected%29%29%20%7B%0A%09%09%09let%20neddy%20%3D%20%7B%7D%3B%0A%09%09%09forEachObjIndexed%28%28value%2C%20key%29%20%3D%3E%20%7B%20%2F%2F%20mapea%20el%20objeto%20como%20array%0A%09%09%09%09neddy%20%3D%20mergeRight%28neddy%2C%20%7B%20%5Bkey%5D%3A%20neddyValue%28value%2C%20origin%29%20%7D%29%3B%0A%09%09%09%7D%2C%20expected%29%3B%0A%09%09%09return%20neddy%3B%0A%09%09%7D%0A%09%09print%28%60%5B%24%7BTAG%7D%5D%20Los%20parametros%20origin%20y%20expected%20no%20pueden%20estar%20vacios%60%29%3B%0A%09%7D%20else%20print%28%60%5B%24%7BTAG%7D%5D%20Los%20parametros%20origin%20y%20expected%20deben%20ser%20objetos%60%29%3B%0A%09return%20%7B%7D%3B%0A%7D%3B%0A%0A%2F%2F%20-----------------%20EJEMPLO%20-------------------%0A%2F%2F%20Objeto%20original%0Aconst%20developer%20%3D%20%7B%0A%20%20nombre%3A%20%27Harley%27%2C%0A%20%20apellidos%3A%20%27Espioza%20Barrias%27%2C%0A%20%20fecha_nacimiento%3A%20%271995-01-08%27%2C%0A%20%20email%3A%20%27harleyespinoza89%40gmail.com%27%2C%0A%20%20direccion%3A%20%7B%0A%20%20%20%20canton%3A%20%27golfito%27%2C%0A%20%20%20%20ciudad%3A%20%27rio%20claro%27%2C%0A%20%20%20%20provincia%3A%20%27puntarenas%27%2C%0A%20%20%20%20pais%3A%20%27Costa%20Rica%27%0A%20%20%7D%2C%0A%20%20pasa_tiempos%3A%20%7B%0A%20%20%20%20deportes%3A%20%7B%0A%20%20%20%20%20%20basket%3A%20%5B%27ver%20juegos%27%5D%2C%0A%20%20%20%20%20%20futbol%3A%20%5B%27jugar%20futbol%27%2C%20%27ver%20juegos%27%5D%0A%20%20%20%20%7D%2C%0A%20%20%7D%0A%7D%3B%0A%0A%2F%2F%20Objeto%20que%20necesito%0Aconst%20neddyObjOfDeveloper%20%3D%20%7B%0A%20%20name%3A%20%27nombre%27%2C%0A%20%20country%3A%20%27direccion.pais%27%2C%0A%20%20familyName%3A%20%27apellido%27%2C%20%2F%2F%20apellido%20no%20existe%2C%20es%20apellidos%0A%20%20job%3A%20%5B%27trabajo%27%2C%20%27Mobile%20Developer%27%5D%2C%0A%20%20hobby%3A%20%5B%27pasa_tiempos.deportes.futbol%27%2C%20%5B%27empty%27%5D%2C%20v%20%3D%3E%20v%5B0%5D%5D%2C%0A%20%20age%3A%20%5B%27fecha_nacimiento%27%2C%20%2750%27%2C%20v%20%3D%3E%20%282019%20-%20parseInt%28split%28%27-%27%2C%20v%29%5B0%5D%2C%2010%29%29%5D%2C%0A%7D%3B%0A%0A%2F%2F%20const%20result%20%3D%20neddyObj%28developer%2C%20%7B%7D%2C%20%27Developer%27%29%3B%0Aconst%20result%20%3D%20neddyObj%28developer%2C%20neddyObjOfDeveloper%2C%20%27Developer%27%29%3B%0Aresult%3B%20%2F%2F%20Presiona%20%22Tidy%22%20en%20la%20consola%20OUTPUT%20y%20verticaliza%20el%20objeto.%0A

*/

import { type, isNil, split, either, pathOr, isEmpty, complement, mergeRight, forEachObjIndexed } from 'ramda';

// validadores
const print = console.warn;
const isArray = entry => (type(entry) === 'Array');
const isString = entry => (type(entry) === 'String');
const isObject = entry => (type(entry) === 'Object');
const isFunction = entry => (type(entry) === 'Function');
const isValidValue = complement(either(isNil, isEmpty));

// @param [entry]: Path en dotCase.
// return Path en formato de array.
const getPath = entry => split('.', entry);

// @param [value]: Valor esperado.
// @param [origin]: Objeto original.
// @return Valor obtenido del path o un valor de remplazo.
const neddyValue = (value, origin) => {
	if (isValidValue(value)) {
		if (isArray(value)) {
			const [expectedValue, replacement, enhancer] = value;
			const result = pathOr(replacement, getPath(expectedValue), origin);
			if (isValidValue(enhancer) && isFunction(enhancer)) return enhancer(result);
			return result;
		}
		return pathOr(undefined, getPath(value), origin);
	}
	return undefined;
};

// @param [origin]: objeto con toda la data original
// @param [expected]: Objeto que necesito y como lo necesito.
// @param [namespace]: Nombre identificador para debug.
// @return Objeto deseado
const neddyObj = (origin, expected, namespace) => {
	const TAG = (namespace && isString(namespace)) ? `NeddyObj-${namespace}` : 'NeddyObj';
	if (isObject(origin) && isObject(expected)) {
		if (!isEmpty(origin) && !isEmpty(expected)) {
			let neddy = {};
			forEachObjIndexed((value, key) => { // mapea el objeto como array
				neddy = mergeRight(neddy, { [key]: neddyValue(value, origin) });
			}, expected);
			return neddy;
		}
		print(`[${TAG}] Los parametros origin y expected no pueden estar vacios`);
	} else print(`[${TAG}] Los parametros origin y expected deben ser objetos`);
	return {};
};

export default neddyObj;
