/* eslint-disable no-underscore-dangle */

import M from './Model';

const _ = undefined;

class UserModel extends M {
	constructor(data: Object = {}) {
		super(data);

		Object.defineProperty(this, '_schema', {
			value: {
				email: M.string(),
				name: M.string(),
				creditCard: M.object({}),
				subscription: M.object({}),
				isKlap: M.boolean()
			},
			enumerable: false
		});

		this.__TYPENAME = 'UserModel';

		this._secure();
	}
}

export default UserModel;
