/* eslint-disable no-underscore-dangle */
import R from 'ramda';

function secure() {
	R.mapObjIndexed((item, key) => {
		// Si el key no existe, indica que se debe tomar tal cual del origin.
		const targetKey = R.defaultTo(key, item.key);

		// TODO AQUI SE DEBE VALIDAR


		// map de objeto
		this[key] = R.defaultTo(item.defaultValue, this._originData[targetKey]);
	}, this._schema);
}

class Model {
	constructor(originData: Object) {
		Object.defineProperty(this, '_originData', { value: originData, enumerable: false });

		Object.defineProperty(this, '_secure', { value: secure, enumerable: false });
	}

	static string(defaultValue: String, key) {
		return { key, type: 'STRING', defaultValue };
	}

	static url(defaultValue: String, key) {
		return { key, type: 'URL', defaultValue };
	}

	static uri(defaultValue: String, key) {
		return { key, type: 'URI', defaultValue };
	}

	static boolean(defaultValue: Boolean, key) {
		return { key, type: 'BOOLEAN', defaultValue };
	}

	static number(defaultValue: Number, key) {
		return { key, type: 'NUMBER', defaultValue };
	}

	static float(defaultValue: Number, key) {
		return { key, type: 'FLOAT', defaultValue };
	}

	static object(defaultValue: Object, key) {
		return { key, type: 'OBJECT', defaultValue };
	}

	static array(defaultValue: Array, key) {
		return { key, type: 'ARRAY', defaultValue };
	}
}

export default Model;
