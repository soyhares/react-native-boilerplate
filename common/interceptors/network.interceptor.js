/* eslint-disable prefer-promise-reject-errors */
// import { NetInfo } from 'react-native';

// const e = { request: { status: -1 } }; const t = 1567; let d = !1; let o = !1; let l = !1;

// function h(p) { o = p; }

// async function s() {
// 	o = await NetInfo.isConnected.fetch();
// 	if (!l) { NetInfo.isConnected.addEventListener('connectionChange', h); l = !0; } return o;
// } s();

// export default {
// 	request: async (c) => {
// 		if (o) return c;
// 		if (!l) { const i = await s(); if (i) return c; }
// 		if (l && !o && !d) { d = !0; setTimeout(() => { d = !1; }, t); return Promise.reject(e); }
// 		return c;
// 	},
// 	response: async r => r
// };

// Agregar <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /> manifest

export default { request: async r => r, response: async r => r };
