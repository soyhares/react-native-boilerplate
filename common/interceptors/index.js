import token from './token.interceptor';
import network from './network.interceptor';
// import tokenCustomer from './token.interceptor.customer';

export default {
	ApiBaum: [
		token,
		network
	],
	ApiCustomer: []
};
