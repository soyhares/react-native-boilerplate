// !NO CAMBIAR A appCore
import Storage from '../../../core/src/Storage';

export default {
	request: async (configuration) => {
		const newConfiguration = configuration;
		const token = await Storage.get(Storage.keys.apiToken);
		if (token) {
			newConfiguration.headers = { ...configuration.headers, Authorization: token };
		}
		return newConfiguration;
	},
	response: async (response) => {
		if (response.data.token) {
			await Storage.set(Storage.keys.apiToken, response.data.token);
		}
		return response;
	}
};

