import axios from 'axios';
import encryptData from './encrypt';
import formater from './formater';
import endpoints from './endpoints';

// const Api = axios.create({

// });

class GreenPayHelper {
	static formatCard(cardDataForm) {
		const card = formater.card(cardDataForm);

		return JSON.stringify(card);
	}

	static encryptCard(publicKey, data) {
		return encryptData(publicKey, data);
	}

	static generatePaymentData(orderData, paymentInfo) {
		// Métodos separados para futuros proyectos.
		// Se formatea, y se encri[ta]
		const card = GreenPayHelper.formatCard(paymentInfo);

		// encriptar información
		const encryptedCard = GreenPayHelper.encryptCard(orderData.publicKey, card);

		// params de greenpay/tokenize
		const payload = formater.cardTokenizeRequest(orderData.session, encryptedCard);

		// Request Headers
		const headers = {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			'liszt-token': orderData.token
		};

		return new Promise((resolve, reject) => {
			axios({
				method: 'post',
				url: endpoints.getCardToken,
				data: payload,
				headers
			}).then(({ data }) => {
				resolve(data);
			}).catch((error) => {
				reject(error);
			});
		});
	}
}

export default GreenPayHelper;
