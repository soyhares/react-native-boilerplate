const card = formData => ({
	card: {
		cardHolder: formData.cardName,
		expirationDate: {
			month: Number(formData.cardExpirationMonth.data),
			year: 21
		},
		cardNumber: formData.cardNumber,
		cvc: formData.cardExpirationCVV,
		nickname: formData.nickname || 'Delfino Payments'
	}
});

// const payInfo = {
// 	card: {
// 		cardHolder: card.name || '',
// 		expirationDate: {
// 			month: card.expirationMonth || '',
// 			year: card.expirationYear || ''
// 		},
// 		cardNumber: card.number || '',
// 		cvc: card.securityCode || ''
// 	}
// };

const cardTokenizeRequest = (session, cardData) => ({
	session,
	ld: cardData.result,
	lk: cardData.keyPairs
});

export default {
	card,
	cardTokenizeRequest
};
