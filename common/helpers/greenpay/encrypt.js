import aesjs from 'aes-js';
import JSEncrypt from 'jsencrypt';

const getAESKeyPairs = () => {
	let iv;
	const key = [];
	for (let k = 0; k < 16; k += 1) {
		key.push(Math.floor(Math.random() * 255));
	}
	for (let k = 0; k < 16; k += 1) {
		iv = Math.floor(Math.random() * 255);
	}
	return ({ k: key, s: iv });
};

const encryptData = (publicKey, data) => {
	const rsa = new JSEncrypt();
	rsa.setPublicKey(publicKey);

	const pair = getAESKeyPairs();

	const textBytes = aesjs.utils.utf8.toBytes(data);
	const aesCtr = new aesjs.ModeOfOperation.ctr(pair.k, new aesjs.Counter(pair.s));

	const encryptedBytes = aesCtr.encrypt(textBytes);
	const encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

	return {
		result: encryptedHex,
		keyPairs: rsa.encrypt(JSON.stringify(pair))
	};
};

export default encryptData;
