export default {
	payOrder: 'https://sandbox-checkout.greenpay.me',
	createOrder: 'https://sandbox-merchant.greenpay.me',
	getRequestToken: 'https://sandbox-merchant.GreenPay.me/tokenize',
	getCardToken: 'https://sandbox-checkout.greenpay.me/tokenize',
	createSubscription: 'https://sandbox-merchant.greenpay.me/subscriptions',
	cancelSubscription: 'https://sandbox-merchant.greenpay.me/subscriptions/cancel',
	listSubscriptions: 'https://sandbox-merchant.greenpay.me/subscriptions/list',
	updateCardTokenSubscription: 'https://sandbox-merchant.greenpay.me/subscriptions/update/card_token',
	updateSubscriptionAmount: 'https://sandbox-merchant.greenpay.me/subscriptions/update',
	subscriptionManualPayment: 'https://sandbox-merchant.greenpay.me/subscriptions/pay',
	listSubscriptionsPayments: 'https://sandbox-merchant.greenpay.me/subscriptions/list/payments'
};
