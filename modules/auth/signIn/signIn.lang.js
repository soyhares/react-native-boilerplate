export default {
	es: {
		header: 'Iniciar sesión',
		submit: 'ENVIAR',
		caption: 'Potenciamos marcas a través de estrategias de comuniación digital.',
		buttons: {
			google: 'Iniciar con Google',
			facebook: 'Iniciar con Facebook',
			default: 'Iniciar con E-mail',
			guest: 'Continuar como invitado'
		},
		assistance: {
			title: '¿Necesita asistencia?',
			desc: {
				textBefore: 'Escríbanos a',
				link: ' contacto@baumdigital.com ',
				textAfter: 'y con mucho gusto le solucionamos.'
			}
		},
		footer: {
			text: 'Si aún no tiene cuenta',
			link: ' Regístrese. '
		}
	},
	en: {
		header: 'Log in',
		submit: 'SEND',
		caption: 'We promote brands through digital communication strategies.',
		buttons: {
			google: 'Continue with Google',
			facebook: 'Continue with Facebook',
			default: 'Continue with E-mail',
			guest: 'Continue as guest'
		},
		assistance: {
			title: 'Do you need assistance?',
			desc: {
				textBefore: 'Write to us',
				link: ' contacto@baumdigital.com ',
				textAfter: 'and we will gladly solve it.'
			}
		},
		footer: {
			text: 'If you do not have an account yet',
			link: ' Register. '
		}
	}
};
