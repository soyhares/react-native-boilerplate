import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Page, Button, Div, Paragraph, Surface, Footer, TextWithLink, ProgressBar } from 'appComponents';
import { Icons } from 'appCommon';
import { Title } from 'customComponents';

const photo = '';
const session = !__DEV__ ? {} : { name: 'Harley', familyName: 'Espinoza', email: 'harley@baum.digital', photo }

class AuthSignInUI extends Component {
	state = { loading: false };

	onSignin = async () => {
		this.setState({ loading: true });
		await this.props.signInWithSocialNetwork(session);
		this.setState({ loading: false }, () => this.props.navigation.navigate('_app'));
	}

	onFacebookPress = () => {
		this.onSignin();
	}

	onGooglePress = () => {
		this.onSignin();
	}

	onEmailPasswordPress = () => this.props.navigation.navigate('_authSignInWithEmail');

	onContinueAsGuest = () => {
		this.props.navigation.navigate('_guest');
	}

	subscribe = () => alert('PRONTO');

	sendEmail = () => alert('PRONTO');

	render() {
		const { Language, navigation: { getParam } } = this.props;
		const hasHeader = getParam('hasHeader', false); // TODO La navegacion no trae state.
		const header = hasHeader ? Page.header.back() : undefined;
		return (
			<Page
				staticBottom={(
					<Footer>
						<TextWithLink
							ai="center"
							link={this.subscribe}
							textLink={Language.footer.link}
							textBefore={Language.footer.text}
							textBeforeProps={{ color: 'textOnPrimary', type: 'bold' }} />
					</Footer>
				)}
				staticTop={(
					<ProgressBar visible={this.state.loading} indeterminate />
				)}
				header={header}
				devName="AuthSignInUI">

				<Title>{Language.header}</Title>

				<Paragraph>{Language.caption}</Paragraph>

				<Surface pb={hasHeader ? 'x4' : 'x0'}>
					<Button
						icon={Icons.google}
						upperText={false}
						color="secondary"
						onPress={this.onGooglePress}
						title={Language.buttons.google} />

					<Button
						icon={Icons.facebook}
						upperText={false}
						color="facebook"
						onPress={this.onFacebookPress}
						title={Language.buttons.facebook} />

					<Button
						icon={Icons.email}
						upperText={false}
						iconFamily="MaterialIcons"
						onPress={this.onEmailPasswordPress}
						title={Language.buttons.default} />
					
					<Div visible={!hasHeader}>
						<Button
							transparent
							size="sm"
							type="bold"
							upperText={false}
							onPress={this.onContinueAsGuest}
							title={Language.buttons.guest} />
					</Div>
					
				</Surface>

				<Title>{Language.assistance.title}</Title>

				<TextWithLink
					mh="x5"
					link={this.sendEmail}
					textLink={Language.assistance.desc.link}
					textAfter={Language.assistance.desc.textAfter}
					textBefore={Language.assistance.desc.textBefore} />
			</Page>
		);
	}
}

AuthSignInUI.propTypes = {
	Language: PropTypes.object.isRequired,
	navigation: PropTypes.object.isRequired,
	signInWithSocialNetwork: PropTypes.func.isRequired
};

export default AuthSignInUI;
