import { connect, signInWithSocialNetwork } from 'appRedux/actions';
import { languageSelector, createStructuredSelector } from 'appRedux/selectors';
import AuthSignInUI from './signIn.iu';
import lang from './signIn.lang';

export default connect(createStructuredSelector({
	Language: languageSelector(lang)
}), { signInWithSocialNetwork })(AuthSignInUI);
