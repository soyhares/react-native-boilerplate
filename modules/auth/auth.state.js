import { AUTH_SET_SESSION } from 'appRedux/types/auth';

const DF_STATE = {
	hasNotSignedIn: true,
	session: {}
};

export default (state = DF_STATE, { type, payload }) => {
	switch (type) {
		case AUTH_SET_SESSION: return ({ ...state, ...payload });
		default: return state;
	}
};
