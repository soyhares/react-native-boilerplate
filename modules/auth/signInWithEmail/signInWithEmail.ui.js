import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
	Page,
	Surface,
	Paragraph,
	Formik,
	Button
} from 'appComponents';

import validation from './validationSchema';

export default class SignInWithEmailUI extends Component {
	static propTypes = {
		Language: PropTypes.object.isRequired,
		navigation: PropTypes.object.isRequired
	};

	constructor(props) {
		super(props);
		this.recoverForm = null;
		this.state = {};
	}

	submit = values => alert(JSON.stringify(values, null, 2));

	render() {
		const { Language } = this.props;
		const validationSchema = validation(Language.form);
		return (
			<Page
				header={Page.header.backTitle(Language.header)}
				devName="SignInWithEmailUI">
				<Surface>
					<Formik
						validationSchema={validationSchema}
						onSubmit={this.submit}
						render={formikProps => (
							<Formik.Form style={{ padding: 10 }}>
								<Formik.Input
									name="email"
									type="email"
									placeholder={Language.form.email.placeholder}
									error={formikProps.errors.email} />
								<Formik.Input
									name="password"
									type="password"
									placeholder={Language.form.password.placeholder}
									error={formikProps.errors.password} />
								<Button onPress={formikProps.handleSubmit} title={Language.submit} />
								<Formik.Switch
									initValue={false}
									name="displayLog"
									label={Language.log} />
								<Paragraph divProps={{ mv: 'x3', visible: formikProps.values.displayLog === true }} color="gray">{JSON.stringify(formikProps, null, 2)}</Paragraph>
							</Formik.Form>
						)} />

				</Surface>

			</Page>
		);
	}
}
 
