import * as Yup from 'yup';

export default LanguageForm => (
	Yup.object().shape({
		email: Yup.string()
			.required(LanguageForm.required)
			.email(LanguageForm.email.error),
		password: Yup.string()
			.required(LanguageForm.required)
			.min(6, LanguageForm.password.length)
	})
);
