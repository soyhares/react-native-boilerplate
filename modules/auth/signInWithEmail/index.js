import { authSignIn, connect } from 'appRedux/actions';
import { languageSelector, createStructuredSelector } from 'appRedux/selectors';
import SignInWithEmail from './signInWithEmail.ui';
import lang from './signInWithEmail.lang';

export default connect(createStructuredSelector({
	Language: languageSelector(lang)
}), { authSignIn })(SignInWithEmail);
