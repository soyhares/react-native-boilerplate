export default {
	es: {
		submit: 'Ingresar',
		header: 'Iniciar sesión',
		log: 'Motrar logs',
		description: 'Ingrese el correo electrónico o identificación con el que se registró y recibirá un enlace para crear una nueva contraseña.',
		form: {
			required: 'Este campo es requerido',
			email: {
				placeholder: 'Correo electrónico',
				error: 'Correo invalido'
			},
			password: {
				placeholder: 'Contraseña',
				length: 'La contraseña no es segura.'
			}
		}
	},
	en: {
		submit: 'Enter',
		header: 'Log in',
		log: 'Display logs',
		description: 'Enter the email or identification with which you registered and you will receive a link to create a new password.',
		form: {
			required: 'This field is required',
			email: {
				placeholder: 'Email',
				error: 'Invalid email '
			},
			password: {
				placeholder: 'Password',
				length: 'The password is not secure.'
			}
		}
	}
};
