import { LAYOUT_HIDE_GLOBAL_SPINNER, LAYOUT_SHOW_GLOBAL_SPINNER } from '../../redux/types/layout';

const DF_SATE = {
	spinnerVisible: false
};

export default (state = DF_SATE, { type }) => {
	switch (type) {
		case LAYOUT_HIDE_GLOBAL_SPINNER: {
			return { spinnerVisible: false };
		}

		case LAYOUT_SHOW_GLOBAL_SPINNER: {
			return { spinnerVisible: true };
		}

		default: return state;
	}
};
