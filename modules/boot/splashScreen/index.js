import { connect, loadDataInStorage } from 'appRedux/actions';
import SplashScreenUI from './splashScreen.ui';

const mstp = ({ auth }) => ({ auth });

export default connect(mstp, { loadDataInStorage })(SplashScreenUI);
