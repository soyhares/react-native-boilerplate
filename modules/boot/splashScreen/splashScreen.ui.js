import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, StatusBar, View } from 'react-native';
import { Spinner, Div, LocalImage } from 'appComponents';
import { Timer } from 'appCommon';
import { withTheme } from 'appTheme';

const Stl = StyleSheet.create({
	wrapper: backgroundColor => ({
		backgroundColor,
		flex: 1,
		justifyContent: 'center'
	}),
	logo: {
		width: 250,
		height: 160
	}
});

class SplashScreenUI extends Component {
	componentWillMount() {
		StatusBar.setHidden(true);
		StatusBar.setBarStyle('light-content');
		StatusBar.setBackgroundColor(this.props.theme.palette.accent);
	}

	componentDidMount = async () => {
		const { loadDataInStorage, navigation: { navigate } } = this.props;
		const { hasNotChoosedLanguage, hasNotSignedIn, hasNotWatchedTips } = await loadDataInStorage();
		Timer.setTimeout(() => {
			switch (true) {
				case hasNotChoosedLanguage: return navigate('_lang');
				case hasNotSignedIn: return navigate('_auth');
				case hasNotWatchedTips: return navigate('_tips')
				default: return navigate('_app');
			}
		}, 2000);
	};

	render() {
		const { palette: { white } } = this.props.theme;
		return (
			<View style={Stl.wrapper(white)}>
				<Div ai="center">
					<LocalImage image={LocalImage.images.logo} style={Stl.logo}/>
				</Div>
				<Div ai="center">
					<Div style={{ width: '50%' }}>
						<Spinner />
					</Div>
				</Div>
			</View>
		);
	}
}

SplashScreenUI.propTypes = {
	navigation: PropTypes.object.isRequired,
	loadDataInStorage: PropTypes.func.isRequired
};

const SplashScreenUIWithTheme = withTheme(SplashScreenUI)

export default SplashScreenUIWithTheme;
