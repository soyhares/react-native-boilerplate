import React from 'react';
import PropTypes from 'prop-types';
import { Storage } from 'appCore';
import { Page, Button, Surface } from 'appComponents';

const DevelopmentPage = ({ navigation }) => {
	const onPressClean = async () => {
		await Storage.clear();
		navigation.navigate('_splash');
	};
	const onPressReset = () => navigation.navigate('_splash');
	const { getParam } = navigation;
	const hasHeader = getParam('hasHeader', false);
	const header = hasHeader ? Page.header.backTitle('DEV MENU') : undefined;
	return (
		<Page devName="DevelopmentPage" header={header}>
			<Surface>
				<Button title="RESET STORAGE" onPress={onPressClean} />
				<Button title="RESTART" onPress={onPressReset} />
			</Surface>
		</Page>
	);
};

DevelopmentPage.propTypes = { navigation: PropTypes.object.isRequired };

export default DevelopmentPage;
