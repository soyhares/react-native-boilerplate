// import { contains } from 'ramda';
// import DeviceInfo from 'react-native-device-info';
import { SETTINGS_SET_SESSION } from 'appRedux/types/settings';

// log('SYSTEM_LANGUAGE_DETECTED', DeviceInfo.getDeviceLocale());

const DF_STATE = {
	languageCode: 'es',
	hasNotChoosedLanguage: true
};

export default (state = DF_STATE, { type, payload }) => {
	switch (type) {
		case SETTINGS_SET_SESSION:
			return { ...state, ...payload };
		default:
			return state;
	}
};
