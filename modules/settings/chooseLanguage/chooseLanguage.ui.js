import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withTheme, StyleSheet } from 'appTheme';
import { Page, Button, Div, Surface, Paragraph, LocalImage, ProgressBar } from 'appComponents';

const Stl = StyleSheet.create({
	logo: {	width: 107,	height: 68 },
	wrapper: backgroundColor => ({ backgroundColor,	flex: 1, justifyContent: 'flex-end'	})
});

const ChooseLanguageUI = ({ theme: { palette }, navigation: { getParam, navigate }, Language, setLanguage }) => {
	const [loading, setLoading] = useState(false);
	const hasHeader = getParam('hasHeader', false);
	const header = hasHeader ? Page.header.backTitle(Language.header) : undefined;
	const changeLanguage = async (languageCode) => {
		setLoading(true);
		await setLanguage(languageCode);
		setLoading(false);
		navigate('_auth');
	};
	return (
		<Page
			barStyle="dark"
			header={header}
			scrollable={false}
			devName="ChooseLanguageUI"
			statusBarColor={palette.white}
			staticTop={(
				<ProgressBar visible={loading} indeterminate />
			)}>
			<LocalImage image={LocalImage.images.build} style={Stl.wrapper(palette.white)}>
				<Div ai="center">
					<LocalImage image={LocalImage.images.logo} style={Stl.logo} />
					<Paragraph>{Language.title}</Paragraph>
				</Div>
				<Surface color="white">
					<Button title="Español" onPress={() => changeLanguage('es')} />
					<Button title="English" onPress={() => changeLanguage('en')} />
				</Surface>
			</LocalImage>
		</Page>
	);
};

ChooseLanguageUI.propTypes = {
	theme: PropTypes.object.isRequired,
	Language: PropTypes.object.isRequired,
	setLanguage: PropTypes.func.isRequired,
	navigation: PropTypes.object.isRequired
}

const ChooseLanguageUIWithTheme = withTheme(ChooseLanguageUI);

export default ChooseLanguageUIWithTheme;
