import { connect, setLanguage } from 'appRedux/actions';
import { sessionSelector, createStructuredSelector, languageSelector } from 'appRedux/selectors';
import ChooseLanguageUI from './chooseLanguage.ui';
import lang from './chooseLanguage.lang';

export default connect(createStructuredSelector({
	session: sessionSelector,
	Language: languageSelector(lang)
}), { setLanguage })(ChooseLanguageUI);
