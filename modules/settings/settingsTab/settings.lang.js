export default ({
	es: {
		menuOptions: {
			signIn: 'Iniciar sesión',
			account: 'Mi cuenta',
			notifications: 'Recibir Notificaciones',
			aboutUs: 'Acerca de Baum Digital',
			help: 'Ayuda',
			signOut: 'Cerrar Sesión'
		},
		sessionBar: {
			emptyLabel: 'Subir foto',
			joinUS: 'Crea tu cuenta',
			goSignUp: 'Ir a crear mi cuenta'
		},
		version: 'Versión'
	},
	en: {
		menuOptions: {
			signIn: 'Sign in',
			account: 'My account',
			notifications: 'Receive Notifications',
			aboutUs: 'About Baum Digital',
			help: 'Help',
			signOut: 'Close Session'
		},
		sessionBar: {
			emptyLabel: 'Photo',
			joinUS: 'Create your account',
			goSignUp: 'Go create my account'
		},
		version: 'Version' 
	}
});
