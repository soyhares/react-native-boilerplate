/* eslint-disable dot-notation */
import { isEmpty, union, propEq, filter, map, pathOr } from 'ramda';
import { Icons, Razor } from 'appCommon';

const NONE = 'none'; // sin role
const PRIVATE = 'private'; // con una sesión
const PUBLIC = 'public'; // como invitado

// ? SE PUEDEN AGREGAR MAS ITEMS AQUÍ
const menuOptions = [
	{ name: 'help', order: 4, icon: Icons.cuestionCircle, role: NONE },
	{ name: 'aboutUs', order: 3, icon: Icons.logoCircle, role: NONE },
	{ name: 'signIn', order: 0, icon: Icons.userCircle, role: PUBLIC },
	{ name: 'account', order: 1, icon: Icons.userCircle, role: PRIVATE },
	{ name: 'signOut', order: 5, icon: Icons.doorCircle, role: PRIVATE },
	{ name: 'notifications', order: 2, icon: Icons.bellCircle, role: NONE, exception: true } // TODO 'exception' le indica al menu que se comporte como switch. Podría ser más dinamico el item
];

const toAssignBadgeAndValue = (item, badges, values) => {
	const badge = pathOr(0, [item.name], badges);
	const value = pathOr(undefined, [item.name], values);
	return ({ ...item, badge, value });
};

export default ({ session, values, badges }) => {
	const optionsGuest = filter(propEq('role', PUBLIC), menuOptions); // Extrae los items con role publico
	const optionsToUser = filter(propEq('role', PRIVATE), menuOptions);// Extrae los items con role privado
	const optionsToAll = filter(propEq('role', NONE), menuOptions);// Extrae los items sin role
	const userOptions = isEmpty(session) ? optionsGuest : optionsToUser; // Define si el role de items es privado o publico
	let options = union(userOptions, optionsToAll); // Une los items según el role a los items sin role en un solo listado de items
	options = map(item => toAssignBadgeAndValue(item, badges, values), options); // asigna badges y values
	return Razor.sortingCollectionBy(options, 'order', true); // retorna las opciones segun el order asignado
};
