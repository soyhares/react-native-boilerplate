import React from 'react';
import PropTypes from 'prop-types';
import { Div, Icon } from 'appComponents';
import { Palette, StyleSheet } from 'appTheme';

const Style = StyleSheet.create({
	logo: {
		width: 50,
		height: 50,
		borderWidth: 2,
		borderRadius: 50 / 2,
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: Palette.primary
	}
});

const SocialNetwork = ({ shareWithFacebook, shareWidthTwitter }) => (
	<Div row jc="center" mv="x5">
		<Div style={Style.logo} onPress={shareWithFacebook} mh="x5">
			<Icon name={Icon.names.fbCircle} size={24} color="primary" />
		</Div>
		<Div style={Style.logo} onPress={shareWidthTwitter} mh="x5">
			<Icon name={Icon.names.twitterCircle} size={24} color="primary" />
		</Div>
	</Div>
);

SocialNetwork.propTypes = {
	shareWithFacebook: PropTypes.func.isRequired,
	shareWidthTwitter: PropTypes.func.isRequired
};

export default SocialNetwork;
