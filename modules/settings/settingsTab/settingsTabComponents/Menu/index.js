import { pathOr } from 'ramda';
import PropTypes from 'prop-types';
import { FlatList, Switch } from 'react-native';
import React, { Component } from 'react';
import { Surface, Div, Item, View, Text } from 'appComponents';
import { StyleSheet, Palette, Metrics } from 'appTheme';

const Style = StyleSheet.create({
	printDivider: {
		paddingVertical: 0,
		borderColor: Palette.background,
		borderBottomWidth: Metrics.Width.border
	},
	logo: filledColor => ({
		alignItems: 'center',
		justifyContent: 'center',
		width: Metrics.Width.icon,
		height: Metrics.Height.icon,
		backgroundColor: Palette[filledColor],
		borderRadius: Metrics.Height.icon / 2
	})
});

export default class Menu extends Component {
	static propTypes = {
		actions: PropTypes.object,
		structure: PropTypes.func.isRequired,
		session: PropTypes.object.isRequired,
		language: PropTypes.object.isRequired,
		values: PropTypes.objectOf(PropTypes.bool),
		badges: PropTypes.objectOf(PropTypes.number)
	};

	static defaultProps = {
		actions: {},
		values: { notifications: true },
		badges: { notifications: 1 }
	};

	keyExtractor = (_, index) => String(index);

	renderItem = ({ item }) => {
		const { name } = item;
		const { values, badges, actions, language } = this.props;
		const val = pathOr(undefined, [name], values);
		const alert = pathOr(undefined, [name], badges);
		const title = pathOr(undefined, [name], language);
		const action = pathOr(undefined, [name], actions);
		const isDelfinoLogo = name === 'aboutUs';
		return (
			<Surface mt="x0" mb="x0" mv="x0" pv="x0" style={Style.printDivider}>
				<Item.Menu
					value={val}
					text={title}
					badge={alert}
					hasIcon={!isDelfinoLogo}
					iconProps={{
						size: 14,
						name: item.icon,
						color: 'white',
						filled: 'primary'
					}}
					{...item} // ! Don't move it
					onPress={item.exception ? undefined : action}
					LeftComponent={this.renderDefinoLogo(isDelfinoLogo)}
					RightComponent={this.renderSwitchExeption(item, action)}
				/>
			</Surface>
		);
	};

	renderSwitchExeption = ({ exception, value }, action) => {
		return !exception ? undefined : (
			<Switch
				value={value}
				onValueChange={action}
				thumbColor={value ? Palette.white : Palette.white}
				trackColor={{ false: Palette.silver, true: Palette.primary }}
			/>
		);
	}

	renderDefinoLogo = (display) => {
		if (!display) return undefined;
		return (
			<View style={Style.logo('primary')}>
				<Text size="xxs" type="bold" color="white">BD</Text>
			</View>
		);
	}

	render() {
		const { structure, session, values, badges, ...rest } = this.props;
		const menuOptions = structure({ session, values, badges });
		return (
			<Div mb="x5" {...rest}>
				<FlatList
					data={menuOptions}
					renderItem={this.renderItem}
					keyExtractor={this.keyExtractor}
				/>
			</Div>
		);
	}
}
