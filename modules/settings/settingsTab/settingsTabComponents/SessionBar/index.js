import React from 'react';
import { pathOr, isEmpty } from 'ramda';
import PropTypes from 'prop-types';
import { Surface, ProgressBar, Text, Div, Avatar } from 'appComponents';


const SessionBar = ({ lang, action, session }) => (
	<Surface color="primary" mt="x0" mb="x0" mv="x0">
		{
			isEmpty(session) ? (
				<Div row ai="center">
					<Avatar emptyLabel={lang.emptyLabel} />
					<Div mh="x5" onPress={action}>
						<Text color="white" size="xl" type="bold">
							{lang.joinUS}
						</Text>
						<Text color="primary" type="bold">{lang.goSignUp}</Text>
					</Div>
				</Div>
			) :  (
				<Div row ai="center" onPress={action}>
					<Avatar source={pathOr(undefined, ['photo'], session)} emptyLabel={lang.emptyLabel} />
					<Div mh="x5">
						<Text color="textOnPrimary" size="xl" type="bold">{`${session.name} ${session.familyName}`}</Text>
					</Div>
				</Div>
			)
		}
	</Surface>
);

SessionBar.propTypes = {
	action: PropTypes.func.isRequired,
	lang: PropTypes.object.isRequired,
	session: PropTypes.object.isRequired
};


export default SessionBar;
