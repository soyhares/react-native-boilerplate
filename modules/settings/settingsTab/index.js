import { hideGlobalSpinner, connect, authLogOut, showGlobalSpinner } from 'appRedux/actions';
import { sessionSelector, createStructuredSelector, languageSelector } from 'appRedux/selectors';
import SettingsTabUI from './settings.ui';
import lang from './settings.lang';

export default connect(createStructuredSelector({
	session: sessionSelector,
	Language: languageSelector(lang)
}), { authLogOut, showGlobalSpinner, hideGlobalSpinner })(SettingsTabUI);
