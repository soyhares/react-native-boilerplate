import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Page, Version, ProgressBar } from 'appComponents';
import { AppBar } from 'customComponents';
import handlerOptions from './handlerOptions';
import { Menu, SessionBar, SocialNetwork } from './settingsTabComponents';

class SettingsTabUI extends Component {
	constructor(props) {
		super(props);
		this.hasHeader = { hasHeader: true };
		this.state = { acceptPushNotification: true, loading: false }; // ! conectar a session
	}

	userLogout = async () => {
		this.setState({ loading: true });
		await this.props.authLogOut();
		this.setState({ loading: false }, () => this.props.navigation.navigate('_guest'));
	}

	onNotificationChange = () => {
		const { acceptPushNotification } = this.state;
		this.setState({ acceptPushNotification: !acceptPushNotification });
	}

	optionsActions = () => ({
		signIn: () => this.props.navigation.navigate('_guestAuth', this.hasHeader),
		account: () => this.props.navigation.navigate('_settingsAccount', this.hasHeader),
		notifications: this.onNotificationChange,
		signOut: this.userLogout
	});

	render() {
		const { Language, session, navigation: { navigate } } = this.props;
		return (
			<Page
				staticTop={(
					<>
						<SessionBar
							session={session}
							lang={Language.sessionBar}
							action={() => alert('Pronto')} />
						<ProgressBar indeterminate visible={this.state.loading} />
					</>
				)}
				devName="SettingsTabUI">
				<Menu
					values={{
						notifications: this.state.acceptPushNotification
					}}
					badges={{
						notifications: this.state.acceptPushNotification ? 1 : 0
					}}
					session={session}
					structure={handlerOptions}
					actions={this.optionsActions()}
					language={Language.menuOptions} />
				<Version label={Language.version} onPress={() => this.props.navigation.navigate('_devPage', this.hasHeader)} />

				<SocialNetwork
					shareWidthTwitter={() => alert('Pronto')}
					shareWithFacebook={() => alert('Pronto')} />

			</Page>
		);
	}
}

SettingsTabUI.propTypes = {
	Language: PropTypes.object.isRequired,
	showGlobalSpinner: PropTypes.func.isRequired,
	navigation: PropTypes.object.isRequired,
	authLogOut: PropTypes.func.isRequired,
	session: PropTypes.object.isRequired
};

export default SettingsTabUI;
