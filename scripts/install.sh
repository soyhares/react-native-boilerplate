#!/bin/bash

# ONLY_OUTPUTS

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
yellow=`tput setaf 3`
# echo "${red}red text ${green}green text${reset}"

appPath="./index.js"

echo 'Installing --dev dependencies'

yarn add --dev reactotron-react-native #ONLY_OUTPUTS
yarn add --dev plop #ONLY_OUTPUTS

echo "${yellow}=> Configure rn-otron ${reset}"

mkdir config

cat > ./config/otron.js <<EOF
// https://github.com/infinitered/reactotron
import { NativeModules } from 'react-native';
import Reactotron from 'reactotron-react-native';

if (__DEV__) {
	const { scriptURL } = NativeModules.SourceCode;
	const [fromScript] = scriptURL.split('://')[1].split(':');

	Reactotron
		.configure({ name: 'Lealto Reactotron v1', host: fromScript })
		.useReactNative({
			networking: { // optionally, you can turn it off with false.
				ignoreUrls: /symbolicate/
			}
		})
		.connect();

	// LoggerManager.configure({
	// 	useReactotron: true,
	// 	isEnable: __DEV__,
	// 	otron: Reactotron
	// });

	Reactotron.clear();

	global.log = (...p) => Reactotron.log(p);
} else {
	global.log = () => 0;
	// SE PUEDE ACTIVAR PARA PROD, EN LUGAR DE OTRON USA console.log
	// LoggerManager.configure({ isEnable: __DEV__, useReactotron: false });
}
EOF

echo "import './config/otron';" | cat - $appPath > temp && mv temp $appPath

echo 'Installing dependencies'

yarn add react-navigation react-native-reanimated react-native-gesture-handler react-navigation-stack react-navigation-tabs redux react-redux redux-thunk ramda #Vie 12 Jul 2019 09:17:32 PM CST
yarn add react-timer-mixin reselect react-native-animatable prop-types moment link-module-alias #Vie 12 Jul 2019 03:47:12 PM CST
yarn add @callstack/react-theme-provider #Sab 13 Jul 2019 10:17:22 PM CST
yarn add react-native-device-info formik react-native-formik yup recompose currency-formatter react-native-vector-icons  #Mar 16 Jul 2019 04:47:52 PM CST


echo 'Moving templates'

cat > plopfile.js <<EOF
const PlopHelper = require('./src/scripts/plop/helpers');
const PlopGenerator = require('./src/scripts/plop/generators');

module.exports = (plop) => {
	PlopHelper.createdBy(plop);
	PlopHelper.exportReduxAction(plop);
	PlopGenerator.addHeader(plop);
	PlopGenerator.addModule(plop);
	PlopGenerator.addSubmodule(plop);
};
EOF

echo 'Adding Eslint'

# yarn add --dev babel-eslint eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react #ONLY_OUTPUTS

echo 'Deinit src git proyect'
sleep 1

rm -rf ./src/.git

echo 'Update constants file.'
sleep 1
cat > ./config/constants.js <<EOF
export const version = '0.0.1';
EOF

echo 'Update constants file.'
sleep 1
cat > ./config/index.js <<EOF
export { default as Constants } from './constants';
EOF

echo "${green}=====> DONE <=====${reset}"
