const MODULES_PATH = './src/modules';
const REDUCERS_PATH = './src/redux/reducers';
const ACTION_TYPES_PATH = './src/redux/types';
const REDUX_ACTIONS_PATH = './src/redux/actions';

module.exports = {
	MODULES_PATH,
	REDUCERS_PATH,
	ACTION_TYPES_PATH,
	REDUX_ACTIONS_PATH
};
