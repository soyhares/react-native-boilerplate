const { version, name } = require('../info');

const title = `
----- ${name} [v${version}] -----

Choose one to continue:
`;

module.exports = plop => plop.setWelcomeMessage(title);
