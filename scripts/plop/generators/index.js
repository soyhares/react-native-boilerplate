const addModule = require('./addModule');
const addSubmodule = require('./addSubmodule');
const addHeader = require('./addHeader');

module.exports = {
	addModule,
	addHeader,
	addSubmodule
};
