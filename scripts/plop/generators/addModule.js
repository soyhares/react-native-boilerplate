const { inputRequired } = require('../validators');
const { MODULES_PLOP } = require('../plops');
const { MODULES_PATH, ACTION_TYPES_PATH, REDUX_ACTIONS_PATH } = require('../outputs');

module.exports = (plop) => {
	plop.setGenerator('Add module', {
		description: 'Create a project module.',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: 'Enter a name for the project module:',
				validate: inputRequired('Module name')
			},
			{
				type: 'list',
				name: 'moduleType',
				message: 'What kind of module do you need ?',
				default: 'redux',
				choices: [
					{ name: 'Redux', value: 'redux' },
					{ name: 'Nothing', value: 'none' },
					{ name: 'Submodules', value: 'submodule' },
					{ name: 'Submodules & Redux', value: 'submoduleAndRedux' }
				]
			}
		],
		actions: ({ moduleType }) => {
			const actions = [];
			const hasRedux = moduleType === 'redux' || moduleType === 'submoduleAndRedux';
			const hasNotSubmodule = moduleType !== 'submodule' && moduleType !== 'submoduleAndRedux';
			switch (moduleType) {
				case 'submodule': {
					actions.push({
						type: 'add',
						path: `${MODULES_PATH}/{{camelCase name}}/index.js`,
						templateFile: `${MODULES_PLOP}/indexEmpty.hbs`
					});
					break;
				}
				case 'submoduleAndRedux': {
					actions.push({
						type: 'add',
						path: `${MODULES_PATH}/{{camelCase name}}/index.js`,
						templateFile: `${MODULES_PLOP}/indexEmpty.hbs`
					});
					break;
				}
				case 'redux': {
					actions.push({
						type: 'add',
						path: `${MODULES_PATH}/{{camelCase name}}/index.js`,
						templateFile: `${MODULES_PLOP}/indexWithRedux.hbs`
					});
					break;
				}
				default: {
					actions.push({
						type: 'add',
						path: `${MODULES_PATH}/{{camelCase name}}/index.js`,
						templateFile: `${MODULES_PLOP}/index.hbs`
					});
					break;
				}
			}
			if (hasNotSubmodule) {
				actions.push({
					type: 'add',
					path: `${MODULES_PATH}/{{camelCase name}}/{{camelCase name}}.ui.js`,
					templateFile: `${MODULES_PLOP}/${hasRedux ? 'moduleWithRedux.ui.hbs' : 'module.ui.hbs'}`
				});
				actions.push({
					type: 'add',
					path: `${MODULES_PATH}/{{camelCase name}}/{{camelCase name}}.lang.js`,
					templateFile: `${MODULES_PLOP}/module.lang.hbs`
				});
			}
			if (hasRedux) {
				actions.push({
					type: 'add',
					path: `${MODULES_PATH}/{{camelCase name}}/{{camelCase name}}.state.js`,
					templateFile: `${MODULES_PLOP}/module.state.hbs`
				});
				actions.push({
					type: 'add',
					path: `${REDUX_ACTIONS_PATH}/{{camelCase name}}.js`,
					templateFile: `${MODULES_PLOP}/module.action.hbs`
				});
				actions.push({
					type: 'add',
					path: `${ACTION_TYPES_PATH}/{{camelCase name}}.js`,
					templateFile: `${MODULES_PLOP}/module.type.hbs`
				});
			}
			actions.push({
				type: 'add',
				path: `${MODULES_PATH}/{{camelCase name}}/components/index.js`,
				templateFile: `${MODULES_PLOP}/components/index.hbs`
			});
			return actions;
		}
	});
};
