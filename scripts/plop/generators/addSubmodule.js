const { inputRequired } = require('../validators');
const { MODULES_PLOP } = require('../plops');
const { MODULES_PATH } = require('../outputs');

module.exports = (plop) => {
	plop.setGenerator('Add submodule', {
		description: 'Create a submodule for a module.',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: 'Enter a name for the submodule:',
				validate: inputRequired('Submodule name')
			},
			{
				type: 'confirm',
				name: 'hasRedux',
				message: 'Do you need redux?'
			},
			{
				type: 'input',
				name: 'moduleName',
				validate: inputRequired('Module name'),
				message: 'Enter the MODULE name where the submodule will be added:'
			}
		],
		actions: ({ hasRedux }) => {
			const actions = [];
			actions.push({
				type: 'add',
				path: `${MODULES_PATH}/{{camelCase moduleName}}/{{camelCase name}}/index.js`,
				templateFile: `${MODULES_PLOP}/${hasRedux ? 'indexWithRedux.hbs' : 'index.hbs'}`
			});
			actions.push({
				type: 'add',
				path: `${MODULES_PATH}/{{camelCase moduleName}}/{{camelCase name}}/{{camelCase name}}.ui.js`,
				templateFile: `${MODULES_PLOP}/${hasRedux ? 'moduleWithRedux.ui.hbs' : 'module.ui.hbs'}`
			});
			actions.push({
				type: 'add',
				path: `${MODULES_PATH}/{{camelCase moduleName}}/{{camelCase name}}/{{camelCase name}}.lang.js`,
				templateFile: `${MODULES_PLOP}/module.lang.hbs`
			});
			actions.push({
				type: 'append',
				path: `${MODULES_PATH}/{{camelCase moduleName}}/index.js`,
				templateFile: `${MODULES_PLOP}/indexExports.hbs`
			});
			return actions;
		}
	});
};
