const { isEmpty } = require('ramda');

const inputRequired = name => value => (!isEmpty(value) || `${name} is required`);

module.exports = {
    inputRequired
};
