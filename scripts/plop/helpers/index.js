const R = require('ramda');
const M = require('moment');
const FS = require('./FSManager');
const { version, name } = require('../info');
const { REDUX_ACTIONS_PATH } = require('../outputs');


// ? toClassCase: joins text for class name with camel case
// ? try it: https://ramdajs.com/repl/?v=0.26.1#?%2F%2F%20toClassCase%3A%20joins%20text%20for%20class%20name%20with%20camel%20case%0A%2F%2F%20by%3A%20Harley%20Espinoza%0Aconst%20toProperCase%20%3D%20%28text%2C%20isOnlyCamelCase%20%3D%20false%29%20%3D%3E%20%7B%0A%09let%20result%20%3D%20R.split%28%27%20%27%2C%20text%29%3B%0A%09result%20%3D%20R.map%28txt%20%3D%3E%20R.concat%28R.toUpper%28R.head%28txt%29%29%2CR.drop%281%2C%20R.toLower%28txt%29%29%29%2C%20result%29%3B%0A%09result%20%3D%20R.join%28%27%27%2C%20%5BR.toLower%28result%5B0%5D%29%2C%20R.join%28%27%27%2C%20R.drop%281%2C%20result%29%29%5D%29%3B%0A%09if%20%28%21isOnlyCamelCase%29%20%7B%0A%09%09result%20%3D%20R.join%28%27%27%2C%20%5BR.toUpper%28R.head%28result%29%29%2C%20R.drop%281%2C%20result%29%5D%29%3B%0A%09%7D%0A%09return%20result%3B%0A%7D%3B%0A%2F%2F%20text%0Aconst%20txt%20%3D%20%27test%20one%20more%27%3B%0Aconst%20txt2%20%3D%20%27TEST%20ONE%20MORE%27%3B%0Aconst%20txt3%20%3D%20%27TeSt%20onE%20mORe%27%3B%0Aconst%20txt4%20%3D%20%27test%27%3B%0Aconst%20txt5%20%3D%20%27TEST%27%3B%0A%2F%2F%20testing%0Aconst%20test1%20%3D%20toProperCase%28txt%29%3B%0Aconst%20test2%20%3D%20toProperCase%28txt2%2C%20true%29%3B%0Aconst%20test3%20%3D%20toProperCase%28txt3%29%3B%0Aconst%20test4%20%3D%20toProperCase%28txt4%29%3B%0Aconst%20test5%20%3D%20toProperCase%28txt5%29%3B%0A%2F%2F%20output%0Aconst%20print%20%3D%20%7B%0A%20%20test1%2C%0A%20%20test2%2C%0A%20%20test3%2C%0A%20%20test4%2C%0A%20%20test5%0A%7D%3B%0A%0Aprint%3B%0A
const toProperCase = (text, isOnlyCamelCase = false) => {
	let result = R.split(' ', text);
	result = R.map(txt => R.concat(R.toUpper(R.head(txt)),R.drop(1, R.toLower(txt))), result);
	result = R.join('', [R.toLower(result[0]), R.join('', R.drop(1, result))]);
	if (!isOnlyCamelCase) {
		result = R.join('', [R.toUpper(R.head(result)), R.drop(1, result)]);
	}
	return result;
};

const createdBy = (plop) => {
	plop.setHelper('createdBy', () => `\n	Created with ${name} [${version}]\n	LastEdition: ${M().format('LL hh:mm a')}\n`);
};

const writeOnFile = (plop, helperName, path) => {
	plop.setHelper(helperName, (file) => {
		const fileName = toProperCase(file, true);
		const fdPath = `${path}/index.js`;
		const entry = `export * from './${fileName}';\n`;
		FS.writeOnFile({ fdPath, entry });
	});
};

const exportReduxAction = plop => writeOnFile(plop, 'exportReduxAction', REDUX_ACTIONS_PATH);

module.exports = {
	createdBy,
	exportReduxAction
};
