const FS = require('fs');

const isDebug = true
const debugging = isDebug ? console.log : () => isDebug;
const DF_CONFIG = { length: 1000, offset: 0, position: 0 };

class FSManager {
    constructor() {
        this.namespace = 'FSManager';
    }

    info() { debugging(this.namespace); };
 
    writeOnFile({ fdPath, entry }) {
        let fd;
        try {
            fd = FS.openSync(fdPath, 'a');
            FS.appendFileSync(fd, entry, 'utf8');
        } catch (err) {
            debugging({ writeOnFileError: err });
        } finally {
            FS.closeSync(fd || '// !Error');
        }
    }

    copyFile({ fdPath, newFdPath, flag = 'COPYFILE_EXCL' }) {
        // https://nodejs.org/docs/latest-v8.x/api/fs.html#fs_fs_constants_1
        try {
            FS.copyFileSync(fdPath, newFdPath, FS.constants[flag]);
            debugging('copied!');
        } catch (error) {
            debugging({ copyFileError: error });    
        }
    };
}

module.exports = new FSManager();
