# Changelog
Todos los cambios notables a este proyecto serán documentados en este archivo.

El formato se basa en [Mantener un registro de cambios] (https://keepachangelog.com/en/1.0.0/),
y este proyecto se adhiere a [Versiones semánticas] (https://semver.org/spec/v2.0.0.html).

Consulte las etiquetas modificadas, eliminadas, agregadas y corregidas.

## [0.1.2] - Mon Jul 15 12:37:42 CST 2019
### Changed
- Fue cambiado en el plop el formato de las contantes actions y types de redux.
- El archivo currency fue eliminado y en su lugar se creó el componente Money.
- Los componentes fueron ordenados alfabeticamente.

## [0.1.1] - Sat 13 Jul 2019 02:08:05 PM CST
### Added
- Se agregó plantilla de módulo de tipo de acción.
- Se ha añadido la plantilla del módulo de acción.
- Se ha habilitado el generador de submódulos + redux.
- Se agregaron salidas para redux-actions y redux-types.

## [0.0.2] - Sat 13 Jul 2019 12:08:05 PM CST
### Changed
- El helper writeOnFile se cambió para exportar acciones de redux.
- El helper toClassName fue cambiado por el modificador de plop-properCase.

## [0.0.1] - Sat 13 Jul 2019 10:35:15 AM CST
### Added
- Seguimiento inicial de version.json.
- Este archivo CHANGELOG servirá como un ejemplo en evolución de un proyecto de código abierto estandarizado CHANGELOG.
