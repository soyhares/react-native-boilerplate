import Reactotron from 'reactotron-react-native';
import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { ThemeProvider } from 'appTheme';
import { Store, Provider } from 'appRedux';
import { AppRouter, NavigationService } from 'appNavigation';
import SafeAreaView from './components/base/SafeAreaView';
import BlockScreenLoader from './components/base/BlockScreenLoader';

StatusBar.setBarStyle('dark-content');

class App extends Component {
	componentDidMount() {
		// TODO Inicializar Push Notifications
	}

	render() {
		return (
			<SafeAreaView>
				<ThemeProvider>
					<Provider store={Store}>
						<AppRouter onNavigationStateChange={NavigationService.logger} ref={(r) => NavigationService.setTopLevelNavigator(r)} />
						<BlockScreenLoader />
					</Provider>
				</ThemeProvider>
			</SafeAreaView>
		);
	}
}

// Reactotron.overlay
export default __DEV__ ? Reactotron.overlay(App) : App;
