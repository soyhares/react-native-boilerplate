import { combineReducers } from 'redux';
import auth from '../modules/auth/auth.state';
import layout from '../modules/layout/layout.state';
import settings from '../modules/settings/settings.state';

export default combineReducers({
	auth,
	layout,
	settings
});
