import { applyMiddleware, createStore } from 'redux';
import middlewares from './middlewares';
import rootReducer from './reducers';

const Store = createStore(rootReducer, applyMiddleware(...middlewares));

export default Store;
