import { identity } from 'ramda';

import { createSelector, createStructuredSelector } from 'reselect';

export { createStructuredSelector, createSelector, identity };

export const spinnerVisibleSelector = createSelector(
	state => state.layout.spinnerVisible,
	identity
);

export const languageCodeSelector = createSelector(
	state => state.settings.languageCode,
	identity
);

export const languageSelector = lang => createSelector(languageCodeSelector, code => lang[code]);

export const sessionSelector = createSelector(state => state.auth.session, identity);
