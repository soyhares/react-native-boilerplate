import {
	LAYOUT_HIDE_GLOBAL_SPINNER,
	LAYOUT_SHOW_GLOBAL_SPINNER,
} from '../types/layout';

export const showGlobalSpinner = () => LAYOUT_SHOW_GLOBAL_SPINNER();

export const hideGlobalSpinner = () => (dispatch, getState) => {
	const { layout: { spinnerVisible } } = getState();
	if (spinnerVisible) return dispatch(LAYOUT_HIDE_GLOBAL_SPINNER());
	return 0;
};

export const activeGlobalSpinner = (actived = true) => (actived ? showGlobalSpinner() : hideGlobalSpinner());
