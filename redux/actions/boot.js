import { Storage } from 'appCore';
import { AUTH_SET_SESSION } from '../types/auth';
import { SETTINGS_SET_SESSION } from '../types/settings';

const DF_BACKUP = {
	hasNotSignedIn: true,
	hasNotWatchedTips: true,
	hasNotChoosedLanguage: true,
};

const getSettingsBackUp = (dispatch) => new Promise(async (resolve) => {
	const backUpSettings = await Storage.get(Storage.keys.settings);
	if (backUpSettings) {
		dispatch(SETTINGS_SET_SESSION(backUpSettings));
		return resolve(backUpSettings);
	}
	return resolve(DF_BACKUP);
});

const getUserBackUp = dispatch => new Promise(
	async (resolve) => {
		const backUpSession = await Storage.get(Storage.keys.session);
		if (backUpSession) {
			dispatch(AUTH_SET_SESSION(backUpSession));
			resolve(backUpSession);
		}
		return resolve(DF_BACKUP);
	}
);

const loadBackUps = async (dispatch) => {
	const {
		hasNotWatchedTips,
		hasNotChoosedLanguage
	} = await getSettingsBackUp(dispatch);
	const { hasNotSignedIn } = await getUserBackUp(dispatch);
	return ({ hasNotChoosedLanguage, hasNotSignedIn, hasNotWatchedTips });
};

const loadAsyncActions = async (dispatch) => {};

const loadActions = (dispatch) => {};

export const loadDataInStorage = () => (dispatch, getState) => new Promise(async (resolve) => {
	try {
		const { hasNotSignedIn,	hasNotWatchedTips, hasNotChoosedLanguage } = await loadBackUps(dispatch);
		// await loadAsyncActions(dispatch);
		// loadActions()
		resolve({ hasNotChoosedLanguage, hasNotSignedIn, hasNotWatchedTips });
	} catch (error) {
		resolve(DF_BACKUP);
	}
});
