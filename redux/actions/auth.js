import { Storage } from 'appCore';
import { AUTH_SET_SESSION } from '../types/auth';

export const signInWithSocialNetwork = session => dispatch => new Promise(async (resolve) => {
	const payload = { session, hasNotSignedIn: false };
	dispatch(AUTH_SET_SESSION(payload));
	await Storage.set(Storage.keys.session, payload);
	resolve(session);
});

export const authLogOut = () => dispatch => new Promise(async (resolve) => {
	const payload = { session: {}, hasNotSignedIn: true };
	dispatch(AUTH_SET_SESSION(payload));
	await Storage.set(Storage.keys.session, payload);
	resolve({});
});
