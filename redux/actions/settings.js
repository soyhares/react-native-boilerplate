import { Storage } from 'appCore';
import { SETTINGS_SET_SESSION } from '../types/settings';

export const setLanguage = languageCode => dispatch => new Promise(async (rs) =>{
	const payload = { languageCode, hasNotChoosedLanguage: false };
	dispatch(SETTINGS_SET_SESSION(payload));
	await Storage.set(Storage.keys.settings, payload);
	rs(languageCode);
});
