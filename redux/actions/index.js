export { connect } from 'react-redux';
export const dispatch = a => d => d(a);
export * from './auth';
export * from './layout';
export * from './boot';
export * from './settings';
