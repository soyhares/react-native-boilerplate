export const LAYOUT_SHOW_GLOBAL_SPINNER = payload => ({
	type: LAYOUT_SHOW_GLOBAL_SPINNER,
	payload
});

export const LAYOUT_HIDE_GLOBAL_SPINNER = payload => ({
	type: LAYOUT_HIDE_GLOBAL_SPINNER,
	payload
});
