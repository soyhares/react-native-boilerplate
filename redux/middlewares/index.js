import Reactotron from 'reactotron-react-native';
import thunkMiddleware from 'redux-thunk';
import R from 'ramda';

const logger = Reactotron ? (...value) => Reactotron.display({
	name: 'DIFF MIDDLEWARE',
	value: {
		type: value[0],
		payload: value[1],
		diff: value[2]
	},
	preview: value[0]
}) : log;

const isObject = R.compose(R.equals('Object'), R.type);
const allAreObjects = R.compose(R.all(isObject), R.values);
const hasLeft = R.has('left');
const hasRight = R.has('right');
const hasBoth = R.both(hasLeft, hasRight);
const isEqual = R.both(hasBoth, R.compose(R.apply(R.equals), R.values));

const markAdded = R.compose(R.append(undefined), R.values);
const markRemoved = R.compose(R.prepend(undefined), R.values);
const isAddition = R.both(hasLeft, R.complement(hasRight));
const isRemoval = R.both(R.complement(hasLeft), hasRight);

function diff(l, r) {
	return R.compose(
		R.map(R.cond([
			[isAddition, markAdded],
			[isRemoval, markRemoved],
			[hasBoth, R.ifElse(
				allAreObjects,
				R.compose(R.apply(diff), R.values),
				R.values
			)
			]
		])),
		R.reject(isEqual),
		R.useWith(R.mergeWith(R.merge), [R.map(R.objOf('left')), R.map(R.objOf('right'))])
	)(l, r);
}

const objectDiff = R.curry(diff);

const diffMiddleware = store => next => (action) => {
	if (__DEV__) {
		const last = objectDiff(store.getState());
		const result = next(action);
		logger(action.type, action.payload, last(store.getState()));
		return result;
	} return next(action);
};

export default [
	thunkMiddleware,
	diffMiddleware
];
