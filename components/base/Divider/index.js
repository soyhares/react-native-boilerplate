import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { withTheme, StyleSheet } from 'appTheme';

const Divider = ({ fullWidth, theme, visible, mv, style }) => (visible ? (
	<View
		style={[StyleSheet.create({
			divider: {
				width: fullWidth ? '100%' : theme.width.divider,
				backgroundColor: theme.palette.primary,
				height: theme.height.divider,
				marginVertical: theme.grid[mv]
			}
		}).divider, style]}
	/>
) : null);

Divider.propTypes = {
	visible: PropTypes.bool,
	fullWidth: PropTypes.bool,
	theme: PropTypes.object.isRequired
};

Divider.defaultProps = { fullWidth: false, visible: true };

export default withTheme(Divider);
