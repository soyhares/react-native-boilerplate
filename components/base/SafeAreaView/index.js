import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';

const { f1 } = StyleSheet.create({ f1: { flex: 1, backgroundColor: 'black' } });

const MySafeAreaView = props => (
	<SafeAreaView style={f1} {...props} />
);

export default MySafeAreaView;
