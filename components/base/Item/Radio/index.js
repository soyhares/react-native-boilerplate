import React from 'react';
import PropTypes from 'prop-types';
import ItemStyle from '../style';
import Div from '../../Div';
import Icon from '../../Icon';
import Text from '../../Text';

const RadioButton = ({ value, text, textProps, children, contentProps, ...rest }) => (
	<Div style={ItemStyle.container} {...rest}>
		<Div style={ItemStyle.content} jc="flex-start" {...contentProps}>
			<Div row ai="center">
				<Icon
					color="black"
					family="MaterialIcons"
					name={Icon.names[`radio${value ? 'Checked' : ''}`]} />
				<Div ml="x2">
					<Text {...textProps}>{text}</Text>
				</Div>
			</Div>
			{children}
		</Div>
	</Div>
);

RadioButton.propTypes = {
	value: PropTypes.bool,
	textProps: PropTypes.object,
	contentProps: PropTypes.object,
	text: PropTypes.string.isRequired,
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.element])
};

RadioButton.defaultProps = {
	value: true,
	textProps: {},
	contentProps: {},
	children: undefined
};

export default RadioButton;
