import React from 'react';
import PropTypes from 'prop-types';
import { Razor } from 'appCommon';
import ItemStyle from '../style';
import Div from '../../Div';
import Text from '../../Text';
import Icon from '../../Icon';

const Menu = ({ text, styleContainer, badge, hasIcon, styleContent, iconProps, LeftComponent, RightComponent, textProps, contentProps, ...rest }) => (
	<Div style={[ItemStyle.container, styleContainer]} {...rest}>
		<Div jc="space-between" style={[ItemStyle.content, styleContent]} {...contentProps}>
			<Div ai="center" row>
				<Div>
					{LeftComponent}
				</Div>
				<Div visible={hasIcon}>
					<Icon {...iconProps} />
				</Div>
				<Div mh="x5">
					<Text {...textProps}>{text}</Text>
				</Div>
			</Div>
			<Div visible={badge > 0} style={ItemStyle.badge}>
				<Text {...textProps} color="white">{badge}</Text>
			</Div>
			<Div>
				{RightComponent}
			</Div>
		</Div>
	</Div>
);

Menu.propTypes = {
	badge: PropTypes.number,
	hasIcon: PropTypes.bool,
	textProps: PropTypes.object,
	iconProps: PropTypes.object,
	contentProps: PropTypes.object,
	styleContent: PropTypes.object,
	styleContainer: PropTypes.object,
	RightComponent: PropTypes.element,
	LeftComponent: PropTypes.element,
	text: PropTypes.string.isRequired
};

Menu.defaultProps = {
	badge: 0,
	hasIcon: true,
	textProps: {},
	contentProps: {},
	styleContent: {},
	styleContainer: {},
	iconProps: { name: '' },
	LeftComponent: undefined,
	RightComponent: <Icon name={Icon.names.next} />
};

export default Menu;
