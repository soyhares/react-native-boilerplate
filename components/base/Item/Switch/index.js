import React from 'react';
import { Switch } from 'react-native';
import PropTypes from 'prop-types';
import { Palette } from 'appTheme';
import { Razor } from 'appCommon';
import ItemStyle from '../style';
import Div from '../../Div';
import Text from '../../Text';

const SwitchItem = ({ label, initValue, value, textProps, containerProps, contentProps, setFieldValue, ...rest }) => (
	<Div style={ItemStyle.container} {...containerProps}>
		<Div style={ItemStyle.content} {...contentProps}>
			<Text {...textProps}>{label}</Text>
			<Switch
				value={Razor.isValidValue(value) ? value : initValue}
				onValueChange={setFieldValue}
				thumbColor={value ? Palette.white : Palette.white}
				trackColor={{ false: Palette.silver, true: Palette.primary }}
				{...rest}
			/>
		</Div>
	</Div>
);

SwitchItem.propTypes = {
	value: PropTypes.bool,
	initValue: PropTypes.bool,
	textProps: PropTypes.object,
	contentProps: PropTypes.object,
	containerProps: PropTypes.object,
	label: PropTypes.string.isRequired,
	setFieldValue: PropTypes.func.isRequired
};

SwitchItem.defaultProps = {
	initValue: true,
	value: undefined,
	textProps: {},
	contentProps: {},
	containerProps: {}
};

export default SwitchItem;
