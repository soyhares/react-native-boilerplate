import React from 'react';
import PropTypes from 'prop-types';
import ItemStyle from '../style';
import Div from '../../Div';
import Icon from '../../Icon';
import Text from '../../Text';

const Single = ({ text, withIcon, textProps, contentProps, ...rest }) => (
	<Div style={ItemStyle.container} {...rest}>
		<Div style={ItemStyle.content} jc="space-between" {...contentProps}>
			<Text {...textProps}>{text}</Text>
			<Div visible={withIcon}>
				<Icon name={Icon.names.next} family="MaterialIcons" />
			</Div>
		</Div>
	</Div>
);

Single.propTypes = {
	withIcon: PropTypes.bool,
	textProps: PropTypes.object,
	contentProps: PropTypes.object,
	text: PropTypes.string.isRequired
};

Single.defaultProps = {
	withIcon: true,
	textProps: {},
	contentProps: {}
};

export default Single;
