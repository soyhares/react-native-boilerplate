import { StyleSheet, Metrics, Palette } from 'appTheme';

export default StyleSheet.create({
	container: {
		//flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		height: Metrics.Height.item
	},
	content: {
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center'
	},
	badge: {
		alignItems: 'center',
		justifyContent: 'center',
		height: Metrics.Height.badge,
		minWidth: Metrics.Width.badge,
		backgroundColor: Palette.darkred,
		borderRadius: Metrics.Height.badge / 2
	}
});
