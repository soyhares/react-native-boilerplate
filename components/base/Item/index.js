import React from 'react';
import Menu from './Menu';
import Radio from './Radio';
import Check from './Check';
import Custom from './Custom';
import Single from './Single';
import Switch from './Switch';

const Item = props => <Single {...props} />;
Item.Menu = Menu;
Item.Check = Check;
Item.Radio = Radio;
Item.Custom = Custom;
Item.Switch = Switch;
export default Item;
