import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ItemStyle from '../style';
import Div from '../../Div';
import Icon from '../../Icon';
import Text from '../../Text';

class CheckBox extends Component {
	state = { value: this.props.value };

	onChangeValue = value => this.setState({ value: !value }, () => this.props.getValue(!value));

	render = () => {
		const { value } = this.state;
		const { styleContainer, styleContent, text, textProps, children, contentProps, ...rest } = this.props;
		return (
			<Div style={[ItemStyle.container, styleContainer]} onPress={() => this.onChangeValue(value)} {...rest}>
				<Div style={[ItemStyle.content, styleContent]}>
					<Div row ai="center" {...contentProps}>
						<Icon
							family="MaterialIcons"
							color={value ? 'accent' : 'silver'}
							name={Icon.names[`checkBox${value ? 'Checked' : ''}`]} />
						<Div ml="x2">
							<Text {...textProps}>{text}</Text>
						</Div>
					</Div>
					{children}
				</Div>
			</Div>
		);
	}
}

CheckBox.propTypes = {
	value: PropTypes.bool,
	getValue: PropTypes.func,
	textProps: PropTypes.object,
	contentProps: PropTypes.object,
	styleContent: PropTypes.object,
	styleContainer: PropTypes.object,
	text: PropTypes.string.isRequired,
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.element])
};

CheckBox.defaultProps = {
	value: true,
	textProps: {},
	contentProps: {},
	styleContent: {},
	styleContainer: {},
	children: undefined,
	getValue: () => null
};

export default CheckBox;
