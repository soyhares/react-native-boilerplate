import React from 'react';
import PropTypes from 'prop-types';
import ItemStyle from '../style';
import Div from '../../Div';
import Text from '../../Text';

const Custom = ({ text, LeftComponent, RightComponent, textProps, contentProps, ...rest }) => (
	<Div style={ItemStyle.container} {...rest}>
		<Div style={ItemStyle.content} {...contentProps}>
			{LeftComponent}
			<Text {...textProps}>{text}</Text>
			{RightComponent}
		</Div>
	</Div>
);

Custom.propTypes = {
	textProps: PropTypes.object,
	contentProps: PropTypes.object,
	LeftComponent: PropTypes.element,
	RightComponent: PropTypes.element,
	text: PropTypes.string.isRequired
};

Custom.defaultProps = {
	LeftComponent: undefined,
	RightComponent: undefined,
	textProps: {},
	contentProps: {}
};

export default Custom;
