import { Alert } from 'react-native';

const NativeAlert = ({ title, message, buttons, delay = 1000 }) => (
	setTimeout(() => Alert.alert(title, message, buttons), delay)
);

export default NativeAlert;
