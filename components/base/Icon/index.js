import React from 'react';
import PropTypes from 'prop-types';
import { Icons, Razor } from 'appCommon';
import { keys, values } from 'ramda';
import { View } from 'react-native';
import { Palette } from 'appTheme';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import Style from './style';
import icoMoonConfig from './icomoon/selection.json';

const IcoMoon = createIconSetFromIcoMoon(icoMoonConfig);

const AppIcon = ({ filled, name, style, size, color, onPress }) => (
	<View style={Style.container(filled)}>
		<IcoMoon
			name={name}
			size={size}
			onPress={onPress}
			color={Palette[color]}
			style={[Razor.isValidURL(filled), style]}
			hitSlop={{ top: 15, bottom: 15, right: 15, left: 15 }}	/>
	</View>
);

AppIcon.names = Icons;

AppIcon.propTypes = {
	style: PropTypes.object,
	onPress: PropTypes.func,
	filled: PropTypes.oneOf(keys(Palette)),
	name: PropTypes.oneOf(values(Icons)),
	color: PropTypes.oneOf(keys(Palette)),
	size: PropTypes.oneOf([14, 20, 24, 26, 50])
};

AppIcon.defaultProps = {
	style: {},
	size: 24,
	name: null,
	filled: null,
	color: 'primary',
	onPress: undefined
};

export default AppIcon;
