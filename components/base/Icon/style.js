import { Razor } from 'appCommon';
import { StyleSheet, Metrics, Palette } from 'appTheme';

const Style = StyleSheet.create({
	container: filledColor => (Razor.isValidValue(filledColor) ? ({
		alignItems: 'center',
		justifyContent: 'center',
		width: Metrics.Width.icon,
		height: Metrics.Height.icon,
		backgroundColor: Palette[filledColor],
		borderRadius: Metrics.Height.icon / 2
	}) : {})
});

export default Style;
