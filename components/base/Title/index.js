import React from 'react';
import { withTheme } from 'appTheme';
import Text from '../Text';
import Div from '../Div';

const Title = ({ mv, pv, ph, mh, theme, ...rest }) => (
	<Div {...{ mv, pv, ph, mh }}>
		<Text size="xl" type="bold" {...rest} />
	</Div>
);

Title.defaultProps = {};

export default withTheme(Title);
