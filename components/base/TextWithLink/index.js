import React from 'react';
import PropTypes from 'prop-types';
import Div from '../Div';
import Text from '../Text';

const	TextWithLink = ({
	link,
	textLink,
	textAfter,
	textBefore,
	textLinkProps,
	textAfterProps,
	textBeforeProps,
	...divProps
}) => (
	<Div {...divProps}>
		<Text {...textBeforeProps}>
			{textBefore}
			<Text {...textLinkProps} onPress={link}>
				{textLink}
			</Text>
			<Text {...textAfterProps}>
				{textAfter}
			</Text>
		</Text>
	</Div>
);


TextWithLink.propTypes = {
	link: PropTypes.func,
	textLink: PropTypes.string,
	textAfter: PropTypes.string,
	textBefore: PropTypes.string,
	textLinkProps: PropTypes.object,
	textAfterProps: PropTypes.object,
	textBeforeProps: PropTypes.object
};

TextWithLink.defaultProps = {
	textLink: '',
	textAfter: '',
	textBefore: '',
	link: undefined,
	textAfterProps: { type: 'light', size: 'sm' },
	textBeforeProps: { type: 'light', size: 'sm' },
	textLinkProps: { color: 'accent', type: 'bold' }
};

export default TextWithLink;
