import { connect } from 'react-redux';
import { spinnerVisibleSelector, createStructuredSelector } from 'appRedux/selectors';
import BlockScreenLoaderUI from './component';

export default connect(createStructuredSelector({
	visible: spinnerVisibleSelector
}))(BlockScreenLoaderUI);
