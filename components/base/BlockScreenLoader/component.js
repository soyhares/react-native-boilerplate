import React from 'react';
import PropTypes from 'prop-types';
import { Modal, View, ActivityIndicator } from 'react-native';
import { withTheme } from 'appTheme';
import Text from '../Text';
import Div from '../Div';
import Stl from './style';

const denny = () => 0;

const BlockScreenLoaderUI = ({ theme, visible, taskLabel }) => (
	<Modal
		transparent
		visible={visible}
		onRequestClose={denny}>
		<View style={Stl.rootSpModal(theme)}>
			<Div ph="x8" pv="x5" style={Stl.textWrapper(theme)}>
				<Text size="xs" type="bold" color="snow">{taskLabel}</Text>
				<Div mt="x3">
					<ActivityIndicator color={theme.palette.primary} />
				</Div>
			</Div>
		</View>
	</Modal>
);

BlockScreenLoaderUI.propTypes = {
	taskLabel: PropTypes.string,
	theme: PropTypes.object.isRequired,
	visible: PropTypes.bool.isRequired
};

BlockScreenLoaderUI.defaultProps = { taskLabel: 'UN MOMENTO' };

export default withTheme(BlockScreenLoaderUI);
