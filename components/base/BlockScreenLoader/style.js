import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	rootSpModal: theme => ({
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: theme.palette.darkOpacity
	}),
	textWrapper: theme => ({
		backgroundColor: '#000000CC',
		...theme.boxShadow,
		borderRadius: theme.roundness
	})
});
