import React from 'react';
import { keys } from 'ramda';
import PropTypes from 'prop-types';
import { withTheme, Palette } from 'appTheme';
import Div from '../Div';
import { rootSurfaceStyle } from './style';

const Surface = ({ style, children, theme, color, ...rest }) => (
	<Div style={rootSurfaceStyle(theme, style, color)} mt="x4" mb="x3" {...rest}>
		{children}
	</Div>
);

Surface.propTypes = {
	style: PropTypes.object,
	theme: PropTypes.object.isRequired,
	color: PropTypes.oneOf(keys(Palette)),
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.element])
};

Surface.defaultProps = {
	style: {},
	color: 'surface',
	children: undefined
};

export default withTheme(Surface);
