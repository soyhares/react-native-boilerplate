
import { StyleSheet } from 'react-native';

export const rootSurfaceStyle = (theme, style, color) => [
	StyleSheet.create({
		surfaceStyle: {
			backgroundColor: theme.palette[color],
			paddingVertical: theme.grid[theme.spacing.base],
			paddingHorizontal: theme.grid[theme.spacing.base]
		}
	}).surfaceStyle,
	style
];

