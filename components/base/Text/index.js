import React from 'react';
import PropTypes from 'prop-types';
import { memoizeWith, defaultTo, identity, keys, is, toUpper } from 'ramda';
import { Text, StyleSheet, ViewPropTypes } from 'react-native';
import { withTheme, Fonts, Palette } from 'appTheme';
import { Razor } from 'appCommon';

const textStyle = memoizeWith(identity, ([type, size, color, style, center], theme) => [
	StyleSheet.create({
		rootTextStyle: {
			fontSize: defaultTo(theme.font.size.default, theme.font.size[size]),
			fontFamily: defaultTo(
				theme.font.types.default,
				theme.font.types[type]
			),
			color: color ? Palette[color] : theme.palette.textPrimary,
			textAlign: center ? 'center' : undefined
		}
	}).rootTextStyle,
	style
]);

/**
 * Text Component
 * @property {string} size oneOf ['xxs', 'xs', 'sm', 'normal', 'md', 'lg', 'xl', 'xxl', 'xxxl']
 * @property {string} type oneOF []
 */
const MyText = ({ upperCase, center, children, style, color, type, size, theme, ...rest }) => (
	<Text style={textStyle([type, size, color, style, center], theme)} {...rest}>
		{Razor.isValidValue(children) && !is(Array, children) && upperCase ? toUpper(children) : children}
	</Text>
);

MyText.propTypes = {
	center: PropTypes.bool,
	children: PropTypes.node,
	upperCase: PropTypes.bool,
	style: ViewPropTypes.style,
	theme: PropTypes.object.isRequired,
	color: PropTypes.oneOf(keys(Palette)),
	size: PropTypes.oneOf(keys(Fonts.size)),
	type: PropTypes.oneOf(keys(Fonts.types))
};

MyText.defaultProps = {
	type: 'default',
	size: 'normal',
	color: undefined,
	upperCase: false,
	style: undefined,
	center: undefined,
	children: undefined
};

export default withTheme(MyText);
