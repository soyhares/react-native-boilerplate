import M from 'moment';
import { connect } from 'react-redux';
import 'moment/locale/es';
import React from 'react';
import PropTypes from 'prop-types';
import { Razor } from 'appCommon';
import Text from '../Text';
import Div from '../Div';

const { isValidNumber, isValidValue } = Razor;

const Moment = ({ upperCase, time, formatted, format, customFormat, langCode, textProps, ...rest }) => (
	<Div visible={isValidValue(time)} {...rest}>
		<Text upperCase color="silver" {...textProps}>
			{M(isValidNumber(time) ? new Date(time) : time, formatted).locale(langCode).format(isValidValue(customFormat[langCode]) ? customFormat[langCode] : format)}
		</Text>
	</Div>
);


Moment.propTypes = {
	upperCase: PropTypes.bool,
	textProps: PropTypes.object,
	formatted: PropTypes.string, // ? formato en que se envia la fecha o la hora (no enviar nada si es un timestamp)
	customFormat: PropTypes.object,
	langCode: PropTypes.string.isRequired,
	time: PropTypes.oneOfType(PropTypes.string, PropTypes.number),
	format: PropTypes.oneOf(['LT', 'LTS', 'L', 'l', 'LL', 'll', 'lll', 'LLLL', 'llll'])
};

Moment.defaultProps = {
	textProps: {},
	format: 'L',
	upperCase: true,
	formatted: 'L',
	time: M().format('L'),
	customFormat: { es: '', en: '' }
};

const mstp = ({ settings: { languageCode: langCode } }) => ({ langCode });

export default connect(mstp)(Moment);
