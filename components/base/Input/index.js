import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withTheme } from 'appTheme';
import { Animated, View, TouchableWithoutFeedback, TextInput } from 'react-native';
import Text from '../Text';
import Icon from '../Icon';
import Style, { toInput, toPlaceholder, toText, toError } from './style';

class Input extends Component {
	static propTypes ={
		onBlur: PropTypes.func,
		value: PropTypes.string,
		space: PropTypes.number,
		onPress: PropTypes.func,
		error: PropTypes.string,
		label: PropTypes.string,
		editable: PropTypes.bool,
		onChange: PropTypes.func,
		placeholder: PropTypes.string,
		secureTextEntry: PropTypes.bool,
		theme: PropTypes.object.isRequired
	};

	static defaultProps = {
		space: 0,
		value: null,
		error: null,
		label: null,
		editable: true,
		placeholder: null,
		onBlur: undefined,
		onPress: undefined,
		onChange: undefined,
		secureTextEntry: false
	}

	constructor(props) {
		super(props);
		const { secureTextEntry: isSecure } = props;
		this.input = {};
		this.state = { isFocused: false, isSecure };
		this.anime = new Animated.Value(0);
	}

	changeSecure = isSecure => this.setState({ isSecure });

	liveItUp = (time = 200) => {
		const { value } = this.props;
		const { isFocused } = this.state;
		Animated.timing(this.anime, {
			duration: time,
			toValue: (isFocused || (value !== '')) ? 1 : 0
		}).start();
	}

	handle = isFocused => () => this.setState({ isFocused }, this.liveItUp);

	focus = () => this.setState({ isFocused: true }, () => {
		this.liveItUp();
		this.input.focus();
	});

	onBlur = () => this.handle(false)();

	makeRef = (input) => { this.input = input; }

	render() {
		const {
			label,
			space,
			theme,
			value,
			error,
			onPress,
			placeholder,
			secureTextEntry,
			...rest
		} = this.props;
		const { isSecure } = this.state;
		return (
			<TouchableWithoutFeedback onPress={this.focus} style={{ flex: 1 }}>
				<View style={Style.wrapper(theme)}>
					<View style={Style.subWrapper}>
						<View style={toInput(space, value, error, theme)}>
							{
								placeholder ? (
									<Animated.View style={toPlaceholder(this.anime, theme)}>
										<Animated.Text style={toText(this.anime, value, error, theme)}>
											{placeholder}
										</Animated.Text>
									</Animated.View>
								) : null
							}
							<TextInput
								autoCapitalize="none"
								underlineColorAndroid="transparent"
								onFocus={this.handle(true)}
								{...rest}
								ref={this.makeRef}
								onBlur={this.onBlur}
								style={Style.input(theme)}
								secureTextEntry={isSecure} />
						</View>
						{
							value && secureTextEntry
								? (
									<Icon
										color="gray"
										style={Style.icon(theme)}
										name={Icon.names.question}
										onPress={() => this.changeSecure(!isSecure)} />
								) : null
						}
					</View>
					<Text size="sm" type="condensed" color="error" style={toError(space)}>
						{error ? `* ${error}` : ''}
					</Text>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

export default withTheme(Input);
