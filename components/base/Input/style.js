import { StyleSheet } from 'appTheme';

const Style = StyleSheet.create({
	wrapper: ({ grid }) => ({
		marginVertical: grid.x1
	}),
	subWrapper: {
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	radius: ({ height, width, grid }) => ({
		flex: 1,
		justifyContent: 'center',
		borderRadius: grid.h1,
		borderWidth: width.border,
		paddingHorizontal: grid.x1,
		height: height.input + grid.x1
	}),
	top: ({ palette, grid }) => ({
		position: 'absolute',
		left: grid.x1,
		backgroundColor: palette.surface
	}),
	input: ({ palette, height, font, grid }) => ({
		color: palette.input,
		fontSize: font.size.sm,
		justifyContent: 'flex-end',
		height: height.input,
		fontFamily: font.types.default,
		paddingHorizontal: grid.x2
	}),
	text: ({ font, grid }) => ({
		fontSize: font.size.sm,
		fontFamily: font.types.default,
		paddingHorizontal: grid.x2
	}),
	icon: ({ grid }) => ({
		position: 'absolute',
		right: grid.x3,
		top: grid.x3
	})
});

export function toColorText(value, error, { palette }) {
	if (error) return palette.error;
	return (value === '') ? palette.silver : palette.accent;
}

export function toColorView(value, error, { palette }) {
	if (error) return palette.error;
	return (value === '') ? palette.silver : palette.accent;
}

export const toInput = (space, value, error, theme) => (
	StyleSheet.flatten([
		Style.radius(theme),
		{ marginLeft: space },
		{ borderColor: toColorView(value, error, theme) }
	])
);

export const toPlaceholder = (anime, theme) => (
	StyleSheet.flatten([
		Style.top(theme),
		{
			top: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [theme.height.input / 3, -10]
			})
		}
	])
);

export const toText = (anime, value, error, theme) => (
	StyleSheet.flatten([
		Style.text(theme),
		{
			fontSize: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [theme.font.size.sm, theme.font.size.xs]
			})
		},
		{
			color: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [theme.palette.silver, toColorText(value, error, theme)]
			})
		}
	])
);

export const toError = marginLeft => StyleSheet.flatten([{ marginLeft }]);

export default Style;
