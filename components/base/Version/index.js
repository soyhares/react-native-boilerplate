import React from 'react';
import { Config } from 'appCommon';
import Text from '../Text';
import Div from '../Div';

const Version = ({ onPress, label }) => (
	<Div onPress={__DEV__ ? onPress : undefined} mv="x2" ai="center" jc="center">
		<Text color="silver" type="bold" size="xs">{`${label} ${Config.Constants.version}`}</Text>
	</Div>
);

export default Version;
