import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, ViewPropTypes } from 'react-native';
import { Metrics } from 'appTheme';

const G = Metrics.Grid;

const styleProps = {
	row: false,
	pt: undefined,
	pb: undefined,
	pv: undefined,
	pl: undefined,
	pr: undefined,
	ph: undefined,
	mt: undefined,
	mb: undefined,
	mv: undefined,
	ml: undefined,
	mr: undefined,
	mh: undefined,
	ai: undefined,
	jc: undefined,
	f: undefined,
	visible: true
};

const presetStyles = StyleSheet.create({
	row: { flexDirection: 'row' }
});

const divStyle = ({ mv, mh, mt, mb, ml, mr, pb, pt, pv, ph, ai, jc, style, f, row }) => {
	return [
		StyleSheet.create({
			rootDivStyle: {
				marginVertical: G[mv],
				marginHorizontal: G[mh],
				marginTop: G[mt],
				marginBottom: G[mb],
				marginLeft: G[ml],
				marginRight: G[mr],
				paddingVertical: G[pv],
				paddingHorizontal: G[ph],
				paddingTop: G[pt],
				paddingBottom: G[pb],
				flex: f,
				alignItems: ai,
				justifyContent: jc
			}
		}).rootDivStyle,
		row ? presetStyles.row : {},
		style
	];
};

// eslint-disable-next-line react/prefer-stateless-function
class Div extends Component {
	static defaultProps = {
		...styleProps,
		style: {},
		onPress: undefined,
		children: undefined,
		activeOpacity: undefined
	};

	render() {
		const { key, visible, children, style, onPress, activeOpacity, ...otherProps } = this.props;
		if (key === 'test') log(style)
		if (!visible) return null;
		return (
			<TouchableOpacity
				onPress={onPress}
				style={[divStyle(otherProps), style]}
				disabled={onPress === undefined}
				activeOpacity={activeOpacity || 0.6}>
				{children}
			</TouchableOpacity>
		);
	}
}

Div.propTypes = {
	style: ViewPropTypes.style,
	pt: PropTypes.string,
	pb: PropTypes.string,
	pv: PropTypes.string,
	pl: PropTypes.string,
	pr: PropTypes.string,
	ph: PropTypes.string,
	mt: PropTypes.string,
	mb: PropTypes.string,
	mv: PropTypes.string,
	ml: PropTypes.string,
	mr: PropTypes.string,
	mh: PropTypes.string,
	f: PropTypes.number,
	row: PropTypes.bool,
	onPress: PropTypes.func,
	visible: PropTypes.bool,
	activeOpacity: PropTypes.number,
	ai: PropTypes.oneOfType(['center', 'flex-start', 'flex-end']),
	jc: PropTypes.oneOfType(['center', 'space-between', 'space-around']),
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.element])
};

export default Div;
