import React from 'react';
import { values } from 'ramda';
import PropTypes from 'prop-types';
import { Image, ImageBackground } from 'react-native';
import { Razor } from 'appCommon'
import { LocalImages } from 'appCommon';

const LocalImage = ({ children, image, ...rest }) => {
	if (Razor.isValidValue(children)) return (<ImageBackground source={image}  {...rest}>{children}</ImageBackground>);
	return <Image source={image} {...rest} />;
};

LocalImage.images = LocalImages;
LocalImage.propTypes = {
	children: PropTypes.node,
	image: PropTypes.oneOf(values(LocalImages)).isRequired
};
LocalImage.defaultProps = {	children: undefined }

export default LocalImage;
