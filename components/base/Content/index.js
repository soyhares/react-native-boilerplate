import React from 'react';
import { ScrollView } from 'react-native';

// ? La idea es tener aquí todo lo referente a un ScrollView

export default ({ onRefesh, ...rest }) => (
	<ScrollView
		showsVerticalScrollIndicator={false}
		showsHorizontalScrollIndicator={false}
		{...rest} />
);
