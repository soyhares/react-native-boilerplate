import React from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import { withTheme } from 'appTheme';
import Div from '../Div';

const Spinner = ({ theme: { palette: { accent } }, ...rest }) => (
	<Div {...rest}><ActivityIndicator color={accent} /></Div>
);

Spinner.propTypes = { theme: PropTypes.object.isRequired };

const SpinnerWithTheme = withTheme(Spinner);

export default SpinnerWithTheme;
