import React from 'react';
import PropTypes from 'prop-types';
import Text from '../Text';
import Div from '../Div';

const Paragraph = ({ divProps, ...rest }) => (
	<Div {...divProps}><Text {...rest} /></Div>
);

Paragraph.propTypes = { divProps: PropTypes.object };
Paragraph.defaultProps = { divProps: { mh: 'x5', mv: 'x0' } };

export default Paragraph;
