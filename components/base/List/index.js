import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { map, length } from 'ramda';
import Div from '../Div';

export default class List extends Component {
	static propTypes = {
		onChange: PropTypes.func,
		animatedList: PropTypes.bool,
		data: PropTypes.array.isRequired,
		activeItemCondition: PropTypes.func,
		renderItem: PropTypes.func.isRequired
	};

	static defaultProps = {
		animatedList: false,
		activeItemCondition: () => false,
		onChange: itemPressed => itemPressed
	};

	state = {	data: this.props.data };

	componentDidMount = () => {
		if (length(this.props.data) > 0) this.onChange(this.props.data[0]);
	};

	onChange = (itemPressed) => {
		const { activeItemCondition, animatedList } = this.props;
		const { data: optionsBefore } = this.state;
		const data = map(item => ({ ...item, active: activeItemCondition({ item, itemPressed })	}), optionsBefore);
		this.setState({ data }, () => this.props.onChange(itemPressed));
		if (animatedList) this.tabsFilter.scrollToItem({ animated: true, item: itemPressed });
	};

	renderItem = ({ item, index }) => (
		<Div onPress={() => this.onChange(item)}>
			{
				this.props.renderItem({ item, index })
			}
		</Div>
	);

	makeRef = (ref) => { this.tabsFilter = ref; };

	keyExtractor = (_, index) => (String(index));

	render = () => (
		<FlatList
			showsHorizontalScrollIndicator={false}
			showsVerticalScrollIndicator={false}
			{...this.props} // ? Don't move it
			ref={this.makeRef}
			data={this.state.data}
			renderItem={this.renderItem}
			keyExtractor={this.keyExtractor} />
	);
}
