import React from 'react';
import PropTypes from 'prop-types';
import { Image, ImageBackground } from 'react-native';
import { Razor } from 'appCommon'

const ProgresiveImage = ({ children, ...rest }) => {
	if (Razor.isValidValue(children)) return (<ImageBackground {...rest}>{children}</ImageBackground>);
	return <Image {...rest}/>
};

ProgresiveImage.propTypes = { children: PropTypes.node };
ProgresiveImage.defaultProps = {	children: undefined }

export default ProgresiveImage;
