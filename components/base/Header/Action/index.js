import React from 'react';
import { View } from 'react-native';
import { withTheme } from 'appTheme';

// TODO: Poner dentro de un View con flex 1, por si el tema del app no es centrado
// TODO: Conectar las props default al tema

// left: -theme.grid.x1 es para corregir un padding interno de los iconos.
const HeaderAction = ({ enabled, children, theme }) => (
	<View style={{ width: theme.grid.x6, left: -theme.grid.x1 }}>
		{enabled ? children : null}
	</View>
);

export default withTheme(HeaderAction);
