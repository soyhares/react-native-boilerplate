import React from 'react';
import Text from '../../Text';

// TODO: Poner dentro de un View con flex 1, por si el tema del app no es centrado
// TODO: Conectar las props default al tema

const HeaderTitle = ({ text, ...rest }) => (
	<Text size="sm" type="bold" {...rest}>{text}</Text>
);

export default HeaderTitle;
