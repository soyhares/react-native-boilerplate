import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { withTheme } from 'appTheme';
import { Icons } from 'appCommon';
import Icon from '../Icon';
import Text from '../Text';
import Title from './Title';
import Types from './Types';
import Action from './Action';
import Stl from './style';

const Header = ({ theme, config }) => {
	const { type, left = false, center = false, right = false } = config;
	if (type === Types.empty.type) return <View style={Stl.container(theme)} />;
	return (
		<View style={Stl.container(theme)}>
			<Action type="left" enabled={left}>
				<Icon name={Icons.back} onPress={left.onPress} hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }} />
			</Action>
			{center ? <Title text={center.value} /> : null}
			<Action type="right" enabled={right}>
				<Text size="sm" type="bold">B</Text>
			</Action>
		</View>
	);
};

Header.types = Types;

Header.propTypes = {
	theme: PropTypes.object.isRequired,
	config: PropTypes.object.isRequired
};

export default withTheme(Header);
