import { NavigationService } from 'appNavigation';

const types = {
	back: func => ({
		type: 'action',
		iconName: 'back',
		onPress: func || NavigationService.goBack.bind(this)
	}),
	title: value => ({
		type: 'title',
		value
	})
};

const appConfigurations = {
	back: func => ({
		left: types.back(func)
	}),
	backTitle: title => ({
		left: types.back(),
		center: types.title(title)
	}),
	title: title => ({
		center: types.title(title)
	}),
	empty: {
		type: 'empty'
	}
};

export default appConfigurations;
