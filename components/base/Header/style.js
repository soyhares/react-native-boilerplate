import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: theme => ({ 
		backgroundColor: theme.palette.header,
		height: theme.height.header,
		paddingHorizontal: theme.grid[theme.spacing.base],
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	})
});
