import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: ({ height, palette }) => ({
		height: height.footer,
		backgroundColor: palette.footer
	})
});
