import React from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'appTheme';
import Div from '../Div';
import Stl from './style';

const Footer = ({ theme, ...rest }) => (
	<Div
		ai="center"
		jc="center"
		style={Stl.container(theme)}
		{...rest} />
);

Footer.propTypes = { theme: PropTypes.object.isRequired };

export default withTheme(Footer);
