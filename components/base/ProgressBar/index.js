// TODO: Decorar el componente

import * as React from 'react';
import { Animated, Platform, StyleSheet, View } from 'react-native';
import { withTheme } from 'appTheme';

const INDETERMINATE_DURATION = 1500;
const INDETERMINATE_MAX_WIDTH = 0.4;

class ProgressBar extends React.Component {
	static defaultProps = {
		visible: true,
		progress: 0,
	};

	state = {
		width: 0,
		timer: new Animated.Value(0),
		fade: new Animated.Value(0),
	};

	indeterminateAnimation = null;

	componentDidUpdate() {
		const { visible } = this.props;

		if (visible) {
			this._startAnimation();
		} else {
			this._stopAnimation();
		}
	}

	_onLayout = event => {
		const { visible } = this.props;
		const { width: previousWidth } = this.state;

		this.setState({ width: event.nativeEvent.layout.width }, () => {
			// Start animation the very first time when previously the width was unclear
			if (visible && previousWidth === 0) {
				this._startAnimation();
			}
		});
	};

	_startAnimation() {
		const { indeterminate, progress } = this.props;
		const { fade, timer } = this.state;

		// Show progress bar
		Animated.timing(fade, {
			duration: 200,
			toValue: 1,
			useNativeDriver: true,
			isInteraction: false,
		}).start();

		// Animate progress bar
		if (indeterminate) {
			if (!this.indeterminateAnimation) {
				this.indeterminateAnimation = Animated.timing(timer, {
					duration: INDETERMINATE_DURATION,
					toValue: 1,
					// Animated.loop does not work if useNativeDriver is true on web
					useNativeDriver: Platform.OS !== 'web',
					isInteraction: false,
				});
			}

			// Reset timer to the beginning
			timer.setValue(0);

			// $FlowFixMe
			Animated.loop(this.indeterminateAnimation).start();
		} else {
			Animated.timing(timer, {
				duration: 200,
				// $FlowFixMe
				toValue: progress,
				useNativeDriver: true,
				isInteraction: false,
			}).start();
		}
	}

	_stopAnimation() {
		const { fade } = this.state;

		// Stop indeterminate animation
		if (this.indeterminateAnimation) {
			this.indeterminateAnimation.stop();
		}

		Animated.timing(fade, {
			duration: 200,
			toValue: 0,
			useNativeDriver: true,
			isInteraction: false,
		}).start();
	}

	render() {
		const { color, indeterminate, style, theme } = this.props;
		const { fade, timer, width } = this.state;
		const tintColor = color || theme.palette.accent;
		// const trackTintColor = setColor(tintColor)
		// 	.alpha(0.38)
		// 	.rgb()
		// 	.string();
		const trackTintColor = 'rgba(230, 230, 230, 0.38)';
		return (
			<View onLayout={this._onLayout}>
				<Animated.View
					style={[
						styles.container,
						{ backgroundColor: trackTintColor, opacity: fade },
						style,
					]}
				>
					<Animated.View
						style={[
							styles.progressBar,
							{
								backgroundColor: tintColor,
								width,
								transform: [
									{
										translateX: timer.interpolate(
											indeterminate
												? {
													inputRange: [0, 0.5, 1],
													outputRange: [
														-0.5 * width,
														-0.5 * INDETERMINATE_MAX_WIDTH * width,
														0.7 * width
													]
												}
												: {
													inputRange: [0, 1],
													outputRange: [-0.5 * width, 0],
												}
										)
									},
									{
										// Workaround for workaround for https://github.com/facebook/react-native/issues/6278
										scaleX: timer.interpolate(
											indeterminate
												? {
													inputRange: [0, 0.5, 1],
													outputRange: [
														0.0001,
														INDETERMINATE_MAX_WIDTH,
														0.0001
													]
												}
												: {
													inputRange: [0, 1],
													outputRange: [0.0001, 1]
												}
										),
									},
								],
							},
						]}
					/>
				</Animated.View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		height: 4,
		overflow: 'hidden',
	},

	progressBar: {
		flex: 1,
	},
});

export default withTheme(ProgressBar);
