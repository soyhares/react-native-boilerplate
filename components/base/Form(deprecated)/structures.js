
const input = ({
	name,
	language,
	icon = null,
	space = 0,
	show = true,
	maxLength = 50,
	editable = true,
	keyboard = 'default',
	secureTextEntry = false
}) => ({
	// Form
	name,
	show,
	type: 'Input',
	// Component-configuration.
	icon,
	space,
	keyboard,
	editable,
	maxLength,
	secureTextEntry,
	placeholder: language[name].placeholder
});

const dropdown = ({ name, space, language }) => ({
	// Form.
	name,
	show: true,
	type: 'Dropdown',
	// Component-configuration.
	space,
	options: language[name].options,
	placeholder: language[name].placeholder
});

const title = ({ name, typeText = 'bold', language, ...rest }) => ({
	// Form.
	name,
	show: true,
	type: 'Title',
	// Component-configuration.
	typeText,
	...rest
});

export default {
	input,
	title,
	dropdown
};
