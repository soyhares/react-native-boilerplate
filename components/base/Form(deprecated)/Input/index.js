import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Animated, View, TouchableWithoutFeedback, TextInput } from 'react-native';
import Text from '../../Text';
import Icon from '../../Icon';
import Style, { toInput, toPlaceholder, toText, toError } from './style';

export default class Input extends Component {
	static propTypes ={
		onBlur: PropTypes.func,
		icon: PropTypes.string,
		value: PropTypes.string,
		space: PropTypes.number,
		onPress: PropTypes.func,
		error: PropTypes.string,
		label: PropTypes.string,
		editable: PropTypes.bool,
		onChange: PropTypes.func,
		placeholder: PropTypes.string,
		root: PropTypes.object.isRequired,
		refInput: PropTypes.func.isRequired,
		secureTextEntry: PropTypes.bool.isRequired
	};

	static defaultProps = {
		space: 0,
		icon: null,
		value: null,
		error: null,
		label: null,
		editable: true,
		placeholder: null,
		onBlur: undefined,
		onPress: undefined,
		onChange: undefined
	}

	constructor(props) {
		super(props);
		this.refInput = {};
		this.state = { isFocused: false, secureTextEntry: props.secureTextEntry };
		this.anime = new Animated.Value(0);
	}

	componentWillMount() {
		this.liveItUp(0);
	}

	componentDidMount() {
		this.props.refInput(this);
	}

	liveItUp = (time = 200) => {
		const { value } = this.props;
		const { isFocused } = this.state;
		Animated.timing(this.anime, {
			duration: time,
			toValue: (isFocused || (value !== '')) ? 1 : 0
		}).start();
	}

	handle = isFocused => () => this.setState({ isFocused }, this.liveItUp);

	focus = () => this.setState({ isFocused: true }, () => {
		this.liveItUp();
		this.refInput.focus();
	});

	blur = () => {
		if (this.props.onBlur) this.props.onBlur();
		this.handle(false)();
	}

	changeSecure = secureTextEntry => this.setState({ secureTextEntry });

	render() {
		const {
			root,
			icon,
			label,
			space,
			editable,
			value,
			error,
			onPress,
			onChange,
			placeholder,
			...rest
		} = this.props;
		const { secureTextEntry } = this.state;
		return (
			<TouchableWithoutFeedback onPress={this.focus} style={{ flex: 1 }}>
				<View style={Style.wrapper}>
					<View style={Style.subWrapper}>
						<View style={toInput(space, value, error)}>
							{
								placeholder ? (
									<Animated.View style={toPlaceholder(this.anime)}>
										<Animated.Text style={toText(this.anime, value, error)}>
											{placeholder}
										</Animated.Text>
									</Animated.View>
								) : null
							}
							<TextInput
								{...rest}
								onBlur={this.blur}
								style={Style.input}
								autoCapitalize="none"
								{...{
									...root,
									value,
									editable,
									secureTextEntry
								}}
								onChangeText={onChange}
								onFocus={this.handle(true)}
								ref={(c) => { this.refInput = c; }}
								underlineColorAndroid="transparent" />
						</View>
						{
							value && icon
								? (
									<Icon
										name={icon}
										color="gray"
										style={Style.icon}
										family="FontAwesome"
										onPress={() => this.changeSecure(!secureTextEntry)} />
								) : null
						}
					</View>
					<Text size="xxs" type="condensed" color="error" style={toError(space)}>
						{error ? `* ${error}` : ''}
					</Text>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}
