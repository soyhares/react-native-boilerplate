import { StyleSheet, Metrics, Palette, Fonts } from 'appTheme';

const Style = StyleSheet.create({
	wrapper: {
		// flex: 1,
		marginVertical: Metrics.Grid.x1
	},
	subWrapper: {
		// flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	radius: {
		flex: 1,
		justifyContent: 'center',
		borderRadius: Metrics.Grid.h1,
		borderWidth: Metrics.Width.border,
		paddingHorizontal: Metrics.Grid.x1,
		height: Metrics.Height.input + Metrics.Grid.x1
	},
	top: {
		position: 'absolute',
		left: Metrics.Grid.x1,
		backgroundColor: Palette.surface
	},
	input: {
		// flex: 1,
		color: Palette.input,
		// fontSize: Fonts.size.sm,
		justifyContent: 'flex-end',
		height: Metrics.Height.input,
		fontFamily: Fonts.types.default,
		paddingHorizontal: Metrics.Grid.x2
	},
	text: {
		// fontSize: Fonts.size.sm,
		fontFamily: Fonts.types.default,
		paddingHorizontal: Metrics.Grid.x2
	},
	icon: {
		position: 'absolute',
		right: Metrics.Grid.x3,
		top: Metrics.Grid.x3
	}
});

export function toColorText(value, error) {
	if (error) return Palette.error;
	return (value === '') ? Palette.silver : Palette.accent;
}

export function toColorView(value, error) {
	if (error) return Palette.error;
	return (value === '') ? Palette.silver : Palette.accent;
}

export const toInput = (space, value, error) => (
	StyleSheet.flatten([
		Style.radius,
		{ marginLeft: space },
		{ borderColor: toColorView(value, error) }
	])
);

export const toPlaceholder = anime => (
	StyleSheet.flatten([
		Style.top,
		{
			top: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [Metrics.Height.input / 3, -10]
			})
		}
	])
);

export const toText = (anime, value, error) => (
	StyleSheet.flatten([
		Style.text,
		{
			fontSize: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [Fonts.size.sm, Fonts.size.xs]
			})
		},
		{
			color: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [Palette.silver, toColorText(value, error)]
			})
		}
	])
);

export const toError = marginLeft => StyleSheet.flatten([{ marginLeft }]);

export default Style;
