import React from 'react';
import PropTypes from 'prop-types';
import FormRules from './rules';
import FormComponent from './component';
import FormStructures from './structures';
import Input from './Input';
import Dropdown from './Dropdown';

const Form = ({ structure, rules, components, ...rest }) => {
	const FormInitialized = FormComponent(structure, rules, components);
	return <FormInitialized {...rest} />;
};

Form.rules = FormRules;
Form.structures = FormStructures;
// components
Form.Input = Input;
Form.Dropdown = Dropdown;


Form.propTypes = {
	rules: PropTypes.func.isRequired,
	structure: PropTypes.func.isRequired,
	components: PropTypes.func.isRequired
};

export default Form;
