import React from 'react';
import { View } from 'react-native';
import Style from './style';

/**
 * [Mapea la referencia que se busca según criterios]
 * @param  {[Integer]}	options.row		[Número de fila del componente al que se le da 'Enter']
 * @param  {[Integer]}	options.column 	[Número de columna del componente al que se le da 'Enter']
 * @param  {[Object]}	data           	[Objeto que se usa como criterio de comparación]
 * @return {[Boolean]}					[Define si se encontró o no la referencia buscada]
 */
export const mapRef = ({ row, column }, data) => (row === data.row) && (column === data.column);

/**
 * [Inicia los valores de 'data' y 'errores']
 * @param  {[Object]}	oldData		[Datos previos por conservar]
 * @param  {[Array]}	structure	[Estructura del formulario para conocer sus propiedades]
 * @return {[Object]}				[Datos y errores que reflejará el formulario]
 */
export const instantiator = (oldData, structure) => structure.reduce(
	({ data, error }, element) => {
		const { show, type, name } = element;
		if (Array.isArray(element)) {
			const fromRow = instantiator(oldData, element);
			return {
				data: { ...data, ...fromRow.data },
				error: { ...error, ...fromRow.error }
			};
		}
		if (type === 'SubContainer') {
			const fromSubContainer = instantiator(oldData, element.children);
			return {
				data: { ...data, ...fromSubContainer.data },
				error: { ...error, ...fromSubContainer.error }
			};
		}
		if (show) {
			return {
				error: { ...error, [name]: [] },
				data: { ...data, [name]: (oldData[name] || '') }
			};
		}
		return { data, error };
	},
	{ data: {}, error: {} }
);

export function structureComparator(oldStructure, newStructure) {
	const filterStructure = structure => structure.reduce((enabled, element) => {
		let toAdd;
		if (element.type) {
			toAdd = element.show ? [element] : [];
		} else {
			toAdd = filterStructure(element);
		}
		return [...enabled, ...toAdd];
	}, []);
	const result = filterStructure(newStructure);
	return result.length !== oldStructure.length;
}

/**
 * [Valida los datos que se le envían según las reglas del formulario]
 * @param  {[Object]}  			rules 	[Reglas por seguir en cada propiedad]
 * @param  {[Object]}  			data  	[Valores del formulario]
 * @param  {[Object]}  			error 	[Errores del formulario]
 * @param  {[String]}  			name 	[Nombre de la propiedad por evaluar]
 * @param  {[String, Boolean]}	value	[Valor actual por asignar]
 * @return {[Object]}      				[Errores actualizados del formulario]
 */
export function validator(rules, data, error, name, value) {
	if (rules[name] === void 0) return error;

	const { regex, method, compare } = rules[name];
	const toCheck = (value === undefined) ? data[name] : value;

	const comparator = compare && (toCheck !== data[compare.value]);
	const compared = Object.keys(rules)
		.find(e => rules[e].compare && (rules[e].compare.value === name));

	const finder = array => array.reduce((r, { validation, message }) => {
		const toDo = (typeof validation === 'function') ? validation : v => validation.test(v);
		return toDo(toCheck) ? r : [...r, message];
	}, []);

	return {
		...error,
		...compared
			? ({
				[compared]: (toCheck !== data[compared])
					? [rules[compared].compare.message] : []
			}) : ({}),
		[name]: [...(comparator) ? [compare.message] : [], ...finder(regex), ...finder(method)]
	};
}

/**
 * [Componente que renderiza los subcomponentes del formulario]
 * @param {[Array]}		options.array      	[Estructura de componentes por renderizar]
 * @param {[Function]}	options.toRender   	[Método que asigna la lógica al componente por renderizar]
 * @param {[Array]}		options.components 	[Lista de componentes con los que cuenta el formulario]
 * @param {[Integer]}	options.row 		[Índice de la fila por la que se va renderizando en caso de cambiar de dirección a columnas]
 */


export const Generator = ({ array, toRender, components, row }) => (
	<View style={((row === undefined) ? Style.container : Style.row)}>
		{
			array.filter(({ type, show }) => ((!type && (row === undefined)) || show))
				.map((configuration, index, { length }) => {
					const last = (index + 1) === length;
					const key = `${configuration.type || 'Row #'}: ${configuration.name || index}`;
					if (configuration.type) {
						return toRender({
							key,
							configuration,
							direction: {
								last,
								vertical: (row === undefined),
								row: (row === undefined) ? index : row,
								column: (row === undefined) ? 0 : index
							}
						});
					}
					return (
						<Generator
							key={`Generator-${key}`}
							row={index}
							array={configuration}
							{...{ key, toRender, components }} />
					);
				})
		}
	</View>
);
