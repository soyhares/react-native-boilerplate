import { StyleSheet } from 'appTheme';

const Style = StyleSheet.create({
	container: {
		width: '100%'
	},
	row: {
		width: '100%',
		flexDirection: 'row'
	}
});

export default Style;
