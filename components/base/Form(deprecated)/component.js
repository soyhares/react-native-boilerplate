/* eslint-disable react/sort-comp */
import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Razor } from 'appCommon';

import { Generator, mapRef, instantiator, structureComparator, validator } from './helper';

import Div from '../Div';
import Text from '../Text';
import Button from '../Button';

class Form extends Component {
	constructor(props) {
		super(props);

		/**
		 * [Referencias a los componentes del formulario]
		 * @type {Array}
		 */
		this.refInput = [];

		const { data, structure } = this.props;

		/**
		 * [Lógica local del formulario]
		 * @type {Object}
		 * data: Valores de cada subcomponente.
		 * error: Errores de cada subcomponente.
		 * smartReading: Modo de escritura autocorrectiva.
		 */
		this.state = { ...instantiator(data, structure), smartReading: false };
	}

	componentDidMount() {
		this.props.refForm(this);
	}

	// Pendiente de testing.
	componentWillUpdate(nextProps, nextState) {
		const { data } = nextState;
		const { structure } = nextProps;

		const changeStructure = structureComparator(Object.keys(data), structure);
		if (changeStructure) {
			// Si la estructura del formulario cambia, debe reiniciar 'data' y 'error'.
			this.resetData(nextProps);
		}
	}

	componentWillUnmount() {
		this.props.onUnmount();
	}

	/**
	 * Interactions
	 * [Acción que se ejecuta en el botón 'Enter' del teclado]
	 * @param  {[Integer]}	options.row      [Número de fila del componente al que se le da 'Enter']
	 * @param  {[Integer]}	options.column   [Número de columna del componente al que se le da 'Enter']
	 * @param  {[Boolean]}	options.vertical [Indica si el siguiente componente está abajo o al lado del actual]
	 * @param  {[Boolean]}	options.last     [Indica si el componente actual es el último de su fila / columna]
	 */
	onEnter({ row, column, vertical, last }) {
		const itsGoingDown = vertical || last;
		const next = this.getRef({
			row: row + (itsGoingDown ? 1 : 0),
			column: last ? 0 : (column + (itsGoingDown ? 0 : 1))
		});

		if (next && next.focus) next.focus();
		else this.transferData(this.props.onFinish);
	}

	/**
	 * [Acción que se ejecuta cuando se cambia el valor de un componente]
	 * @param  {[String]}	name  [Nombre de la propiedad por editar]
	 * @param  {[type]}		value [Valor actual del componente que cambió]
	 */
	onChange(name, value) {
		const { rules, otherErrors } = this.props;
		const { data, error, smartReading } = this.state;

		this.setState({
			// Cambiar cuando admita dropdowns y datepicker.
			data: { ...data, [name]: value },
			error: smartReading ? validator(rules, data, error, name, value) : error
		}, () => {
			this.props.onChange(value);
			if (Object.keys(otherErrors).length > 0) {
				this.props.resetOtherErrors(otherErrors);
			}
		});
	}

	/**
	 * Reference
	 * [Acceso a la referencia que se busca]
	 * @param  {[Object]} oldR [Criterios de búsqueda]
	 * @return {[Object]}      [Referencia al componente buscado u objeto vacío]
	 */
	getRef(oldR) {
		const toReturn = this.refInput.find(r => mapRef(r, oldR));
		return toReturn ? toReturn.ref : {};
	}

	/**
	 * [Añade referencias al arreglo]
	 * @param {[Object]} newR [Referencia por añadir o sustituir]
	 */
	addRef(newR) {
		const index = this.refInput.findIndex(r => mapRef(r, newR));
		if (index === -1) this.refInput.push(newR);
		else this.refInput[index] = newR;
	}

	/**
	 * Data
	 * [Reinicia el 'data' y 'error' del formulario]
	 * @param  {[Object]} nextProps [Propiedades que refrescan los valores del formulario]
	 */
	resetData({ data, structure } = this.props) {
		this.refInput = [];
		this.setState({ ...instantiator(data, structure) });
	}

	/**
	 * [Valida si puede enviar los datos, si puede, los envía al callback, sino actualiza los errores]
	 * @param  {Function} callback [Método que debe recibir los valores del formulario]
	 */
	transferData(callback) {
		const { data, error } = this.state;
		const { rules, getEmptySpaces, forceOnPress } = this.props;
		// Busco errores en el form.
		const newError = Object.keys(data)
			.reduce((errors, name) => ({
				...errors,
				[name]: validator(rules, data, error, name)[name]
			}), {});
		// Checkeo que no hayan valores vacios.
		const checkData = Object.values(newError).every(errors => errors.length === 0);
		// Cambio el estado.
		this.setState({
			error: newError,
			...(checkData ? {} : { smartReading: true })
		});
		// Salida de la función.
		if (checkData || forceOnPress) {
			callback(getEmptySpaces ? data : Object.keys(data).reduce(
				(object, key) => ({ ...object, ...((data[key] === '') ? {} : ({ [key]: data[key] })) }), {}
			));
		} else if (!this.props.addButton) {
			callback({ error: true });
		}
	}

	// dispatcher
	submit = () => this.transferData(this.props.onFinish)

	/**
	 * Render
	 * [Elige el componente que sigue por renderizar y le añade su lógica]
	 * @param  {[String]}	options.key         [ID del componente]
	 * @param  {[String]}	options.type        [Tipo de componente]
	 * @param  {[String]}	options.name        [Nombre de la propiedad que tiene el valor del componente]
	 * @param  {[String]}	options.placeholder [Texto de titular del componente]
	 * @param  {[Boolean]}	options.required	[Define si el componente es requerido o no (Para efectos visuales)]
	 * @param  {[Boolean]}	options.security	[description]
	 * @param  {[Object]}	options.direction   [Datos que se le pasan al onEnter y addRef para ubicar su referencia]
	 * @return {[Component]}					[Componente por renderizar]
	 */
	renderItem = ({ key, configuration, direction }) => {
		const { data, error } = this.state;
		const { otherErrors } = this.props;
		const { type, name } = configuration;
		const ToReturn = this.props.components[type];
		const { vertical, last, row, column } = direction;
		return (
			<ToReturn
				key={key}
				collapsable={false}
				{...{ configuration }}
				data={{
					value: data[name],
					onChange: v => this.onChange(name, v)
				}}
				method={{
					ref: ref => this.addRef({ row, column, ref }),
					onSubmit: () => this.onEnter({ vertical, last, row, column })
				}}
				error={(error[name] && (error[name].length > 0)) ? error[name][0] : otherErrors[name]} />
		);
	}

	renderSubmitButton = () => {
		const { addButton, language } = this.props;

		if (addButton) {
			const label = language.submitButton || 'Enviar';
			return <Button title={label} onPress={this.submit} />;
		} return null;
	}

	// TODO -> Sacar error a un componente del Form
	render() {
		const { structure, components, style, globalError } = this.props;
		return (
			<View style={style}>
				<Generator
					{...{ components }}
					collapsable={false}
					array={structure}
					toRender={this.renderItem} />
				<Div mv="x2" visible={Razor.isValidValue(globalError)}>
					<Text color="darkred" type="condensed" size="xs">{globalError}</Text>
				</Div>
				{this.renderSubmitButton()}
			</View>
		);
	}
}

Form.propTypes = {
	language: PropTypes.object.isRequired,
	// Estructura.
	rules: PropTypes.object.isRequired,		// Son las reglas de validación de cada input.
	structure: PropTypes.array.isRequired,	// Es la estructura de los input del formulario.
	components: PropTypes.shape({			// Son los componentes que hacen custom al formulario.
		Input: PropTypes.func.isRequired
	}).isRequired,
	// Métodos.
	onFinish: PropTypes.func.isRequired,	// Es la función por ejecutar en el último onSubmit.
	// Opcionales.
	style: PropTypes.object,				// Son los estilos del <View> principal.
	// Estructura.
	data: PropTypes.object,					// Son los valores que tiene cada input.
	otherErrors: PropTypes.object,			// Errores que vienen de lógica externa.
	addButton: PropTypes.bool,				// Determina si se muestra o no el botón de "enviar".
	forceOnPress: PropTypes.bool,			// Determina si se fuerza el 'onPress' aunque haya errores.
	getEmptySpaces: PropTypes.bool,			// Determina si se "envía" el objeto "data" completo, incluyendo espacios vacíos.
	// Métodos.
	refForm: PropTypes.func,				// Es la forma de referenciar al formulario.
	onUnmount: PropTypes.func,				// Método que se ejecuta en el 'componentWillUnmount'.
	onChange: PropTypes.func,				// Método que se ejecuta en el 'onChange' de cualquier componente.
	resetOtherErrors: PropTypes.func,		// Método que se ejecuta en el 'onChange' de cualquier componente (Si 'otherErrors' tiene errores).
	globalError: PropTypes.string, 			// Label que muestra un error ajeno al Form 
};

Form.defaultProps = {
	data: {},
	style: {},
	globalError: '',
	addButton: true,
	otherErrors: {},
	refForm: r => r,
	onChange: r => r,
	forceOnPress: false,
	onUnmount: () => true,
	getEmptySpaces: false,
	resetOtherErrors: r => r
};

export default (s, r, c) => (p = {}) => (
	<Form {...{ ...p, structure: s(p), rules: r(p), components: c(p) }} />
);
