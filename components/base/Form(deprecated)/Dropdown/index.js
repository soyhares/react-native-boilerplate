import IosDropdown from './index.ios';
import AndroidDropdown from './index.android';

export default __IS_IOS__ ? IosDropdown : AndroidDropdown;
