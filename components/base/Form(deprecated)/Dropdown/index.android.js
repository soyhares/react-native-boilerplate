import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { View, Modal, FlatList, TouchableOpacity, TouchableWithoutFeedback, Animated } from 'react-native';
import Icon from '../../Icon';
import Text from '../../Text';
import Style, { toValue, toDropdown, toPlaceholder, toPanel, toItem, toText, toError } from './style.android';

class AppDropdown extends Component {
	constructor(props) {
		super(props);
		this.anime = new Animated.Value(0);
		this.state = { visible: false, x: 0, y: 0, width: 0 };
	}

	componentWillMount() {
		this.liveItUp(0);
	}

	componentDidMount() {
		this.props.refDropdown(this);
	}

	liveItUp = (time = 200) => {
		const { value } = this.props;
		const { visible } = this.state;
		Animated.timing(this.anime, {
			duration: time,
			// toValue: 1
			toValue: (visible || (value !== '')) ? 1 : 0
		}).start();
	}

	focus = () => this.handle(true)();

	handle = visible => () => {
		if (visible && this.view) {
			this.view.measure((fx, fy, width, height, x, y) => (
				this.setState({ visible, x, y, width }, this.liveItUp)
			));
		} else {
			this.setState({ visible }, this.liveItUp);
		}
	}

	onPress = (data, index) => () => {
		this.handle(false)();
		this.props.root.onSelect({ data, index });
	}

	renderModal() {
		const { root, value, allowIntAsString } = this.props;
		const { visible, x, y, width } = this.state;
		return (
			<Modal
				transparent
				visible={visible}
				animationType="fade"
				onRequestClose={this.handle(false)}>
				<TouchableWithoutFeedback onPress={this.handle(false)} style={{ flex: 1 }}>
					<View style={Style.opacity}>
						<View style={toPanel(x, y, width, root.options.length)}>
							<FlatList
								data={root.options}
								keyExtractor={key => `Option #${key}`}
								renderItem={({ item, index }) => (
									<TouchableOpacity
										style={toItem(root.options, item, value, index, allowIntAsString)}
										onPress={this.onPress(root.options[index], index)}>
										<Text color="black">{item}</Text>
									</TouchableOpacity>
								)} />
						</View>
					</View>
				</TouchableWithoutFeedback>
			</Modal>
		);
	}

	render() {
		const { placeholder, required, value, root, space, allowIntAsString, error } = this.props;
		return (
			<TouchableWithoutFeedback onPress={this.handle(true)} style={{ flex: 1 }}>
				<View style={Style.wrapper}>
					<View ref={(view) => { this.view = view; }} style={toDropdown(space, value, error)}>
						{
							placeholder ? (
								<Animated.View style={toPlaceholder(this.anime, value, error)}>
									<Animated.Text style={toText(this.anime, value, error)}>
										{`${placeholder}${required ? ' *' : ''}`}
									</Animated.Text>
								</Animated.View>
							) : null
						}
						<View style={Style.dropdown}>
							<Text color="black">
								{toValue(root.options, value, allowIntAsString)}
							</Text>
							<Icon
								color={value ? 'accent' : 'silver'}
								family="FontAwesome"
								name={Icon.names.down} />
						</View>
					</View>
					{this.renderModal()}
					{
						placeholder ? (
							<Text size="xxs" color="error" style={toError(space)}>
								{error ? `* ${error}` : ''}
							</Text>
						) : null
					}
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

AppDropdown.propTypes = {
	root: PropTypes.object,
	error: PropTypes.string,
	space: PropTypes.number,
	required: PropTypes.bool,
	refDropdown: PropTypes.func,
	placeholder: PropTypes.string,
	allowIntAsString: PropTypes.bool,
	value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.object]).isRequired
};

AppDropdown.defaultProps = {
	root: {},
	space: 0,
	error: null,
	required: false,
	refDropdown: () => {},
	placeholder: undefined,
	allowIntAsString: false
};

export default AppDropdown;
