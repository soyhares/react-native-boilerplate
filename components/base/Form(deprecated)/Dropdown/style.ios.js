import { StyleSheet, Metrics, Palette, Fonts } from 'appTheme';

const Style = StyleSheet.create({
	wrapper: {
		flex: 1,
		marginVertical: Metrics.Grid.x1
	},
	radius: {
		flex: 1,
		borderWidth: 1,
		borderRadius: Metrics.Grid.h1,
		paddingHorizontal: Metrics.Grid.x1,
		height: Metrics.Height.input + Metrics.Grid.x1
	},
	placeholder: {
		// borderWidth: 1,
		position: 'absolute',
		left: Metrics.Grid.x1,
		// borderRadius: Metrics.Grid.x1,
		backgroundColor: Palette.surface,
		paddingHorizontal: Metrics.Grid.x1
	},
	dropdown: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		// borderBottomWidth: 1,
		// borderColor: Palette.divider,
		height: Metrics.Height.input,
		paddingHorizontal: Metrics.Grid.x5,
		justifyContent: 'space-between'
	},
	opacity: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Palette.darkOpacity
		// paddingHorizontal: Metrics.Scaled.Padding.page
	},
	panel: {
		position: 'absolute',
		borderRadius: Metrics.Grid.h1,
		backgroundColor: Palette.white,
		// /height: Metrics.Height.input
	},
	item: {
		flex: 1,
		justifyContent: 'center',
		height: Metrics.Height.input,
		borderColor: Palette.primaryText,
		paddingHorizontal: Metrics.Grid.x1
	},
	text: {
		color: Palette.accent,
		fontFamily: Fonts.types.default

	}
});

export const toValue = (value, options, asString) => {
	switch (true) {
		case (!asString && !isNaN(value)): return options[parseInt(value, 10)];
		case (typeof value === 'object'): return value.data;
		default: return value;
	}
};


export const toPanel = (left, top, width) => {
	const position = { left, width, top };
	return StyleSheet.flatten([Style.panel, position]);
};

export const toItem = (options, item, value, index, asString) => {
	const isSelected = (
		(!asString && !isNaN(value) && (item === options[parseInt(value, 10)]))
		|| ((typeof value === 'object') && (item === value.data))
		|| (item === value)
	);
	const isLast = (index + 1) === options.length;
	return StyleSheet.flatten([
		Style.item,
		{
			borderRadius: isSelected ? Metrics.Grid.h1 : 0,
			borderBottomWidth: (isSelected || isLast) ? 0 : 0.5,
			backgroundColor: isSelected ? Palette.primary : 'transparent'
		}
	]);
}

export function toColorView(value, error) {
	if (error) return Palette.error;
	return (value === '') ? Palette.silver : Palette.accent;
}

export const toDropdown = (space, value, error) => (
	StyleSheet.flatten([
		Style.radius,
		{ marginLeft: space },
		{ borderColor: toColorView(value, error) }
	])
);

export function toColorText(value, error) {
	if (error) return Palette.error;
	return (value === '') ? Palette.silver : Palette.accent;
}

export const toPlaceholder = anime => (
	StyleSheet.flatten([
		Style.placeholder,
		{
			top: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [Metrics.Height.input / 3, -10]
			})
		}
	])
);

export const toText = (anime, value, error) => (
	StyleSheet.flatten([
		Style.text,
		{
			fontSize: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [Fonts.size.sm, Fonts.size.sm]
			})
		},
		{
			color: anime.interpolate({
				inputRange: [0, 1],
				outputRange: [Palette.silver, toColorText(value, error)]
			})
		}
	])
);

export const toError = marginLeft => StyleSheet.flatten([{ marginLeft }]);

export default Style;
