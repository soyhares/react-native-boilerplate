import React, { Component } from 'react';
import { length } from 'ramda';
import { BlockTimer } from 'appCommon';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, ActionSheetIOS, Animated, ActivityIndicator } from 'react-native';

import Icon from '../../Icon';
import Text from '../../Text';
import Style, { toValue, toDropdown, toPlaceholder, toText, toError } from './style.ios';
import Div from '../../Div';

class AppDropdown extends Component {
	constructor(props) {
		super(props);
		this.state = { visible: false, settingUp: false };
		this.anime = new Animated.Value(0);
	}

	componentWillMount() {
		this.liveItUp(0);
	}

	componentDidMount() {
		this.props.refDropdown(this);
	}

	liveItUp = (time = 200) => {
		const { value } = this.props;
		const { visible } = this.state;
		Animated.timing(this.anime, {
			duration: time,
			toValue: (visible || (value !== '')) ? 1 : 0
		}).start();
	}

	focus = () => {
		this.setState({ visible: true, settingUp: true }, this.liveItUp);
		const { options } = this.props.root;
		BlockTimer.execute(() => {
			ActionSheetIOS.showActionSheetWithOptions(
				{ options },
				(index) => {
					this.props.root.onSelect({ data: options[index], index });
					this.setState({ settingUp: false });
				}
			);
		});
	};

	render() {
		const { placeholder, required, value, root, space, allowIntAsString, error } = this.props;
		return (
			<TouchableOpacity onPress={this.focus} style={{ flex: 1 }}>
				<View style={Style.wrapper}>
					<View ref={(view) => { this.view = view; }} style={toDropdown(space, value, error)}>
						{
							placeholder ? (
								<Animated.View style={toPlaceholder(this.anime, value, error)}>
									<Animated.Text style={toText(this.anime, value, error)}>
										{`${placeholder}${required ? ' *' : ''}`}
									</Animated.Text>
								</Animated.View>
							) : null
						}
						{
							this.state.settingUp && length(root.options) > 50
								? (
									<Div pv="x2">
										<ActivityIndicator />
									</Div>
								) : (
									<View style={Style.dropdown}>
										<Text color="accent" size="md">
											{toValue(value, root.options, allowIntAsString)}
										</Text>
										<Icon
											color={value ? 'accent' : 'silver'}
											family="FontAwesome"
											name={Icon.names.down} />
									</View>
								)
						}
					</View>
					{
						placeholder ? (
							<Text size="xxs" color="error" style={toError(space)}>
								{error ? `* ${error}` : ''}
							</Text>
						) : null
					}
				</View>
			</TouchableOpacity>
		);
	}
}

AppDropdown.propTypes = {
	root: PropTypes.object,
	error: PropTypes.string,
	space: PropTypes.number,
	required: PropTypes.bool,
	refDropdown: PropTypes.func,
	placeholder: PropTypes.string,
	allowIntAsString: PropTypes.bool,
	value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.object]).isRequired
};

AppDropdown.defaultProps = {
	root: {},
	space: 0,
	error: null,
	required: false,
	refDropdown: () => {},
	placeholder: undefined,
	allowIntAsString: false
};

export default AppDropdown;

/*
renderOld() {
		const { placeholder, value, required, root, space, allowIntAsString, error } = this.props;
		return (
			<TouchableOpacity onPress={this.focus} style={{ flex: 1 }}>
				<View style={Style.wrapper}>
					<View ref={(view) => { this.view = view; }} style={toBottom(space, value, error)}>
						{
							placeholder ? (
								<Animated.View style={toPlaceholder(this.anime)}>
									<Animated.Text style={toText(this.anime, value, error)}>
										{`${placeholder}${required ? ' *' : ''}`}
									</Animated.Text>
								</Animated.View>
							) : null
						}
						{
							this.state.settingUp && length(root.options) > 50
								? (
									<Div pv="x2">
										<ActivityIndicator />
									</Div>
								) : (
									<View style={Style.dropdown}>
										<Text color="primaryText" size="md">
											{toValue(value, root.options, allowIntAsString)}
										</Text>
										<Icon
											color="primaryText"
											size="tiny"
											family="MaterialIcons"
											name="keyboard-arrow-down" />
									</View>
								)
						}
					</View>
					{
						placeholder ? (
							<Text size="sm" color="red" style={toError(space)}>
								{error ? `* ${error}` : ''}
							</Text>
						) : null
					}
				</View>
			</TouchableOpacity>
		);
	}
	*/
