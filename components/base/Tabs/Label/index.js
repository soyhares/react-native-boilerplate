import React from 'react';
import { withTheme, StyleSheet } from 'appTheme';
import Text from '../../Text';
import Div from '../../Div';

// TODO make it dinamic
const names = {
	_tabHome: 'Dev Menu',
	_tabJoin: 'Dev Menu',
	_tabSettings: 'Ajustes'
};

// ? Necesario para android. Por que si no los labels no se acomodan al centro.
const { rootTabLabelStyle } = StyleSheet.create({
	rootTabLabelStyle: {
		alignSelf: 'center'
	}
});

const Label = withTheme(({ theme, focused, routeName }) => (
	<Div mb="x1" pb="h1">
		<Text style={rootTabLabelStyle} size="xxxs" color={focused ? 'white' : 'gray'}>{names[routeName]}</Text>
	</Div>
));

const HOCTabLabel = routeName => ({ focused }) => (
	<Label focused={focused} routeName={routeName} />
);

export default HOCTabLabel;
