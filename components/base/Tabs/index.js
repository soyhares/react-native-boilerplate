import Icon from './Icon';
import Label from './Label';

export default {
	Icon,
	Label
};
