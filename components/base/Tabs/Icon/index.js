import React from 'react';
import { withTheme } from 'appTheme';
import Div from '../../Div';
import Icon from '../../Icon';

const TabIcon = withTheme(({ theme, focused, routeName }) => (
	<Div>
		<Icon size={routeName === '_tabJoin' ? 26 : 20} color={focused ? 'white' : 'gray'} name={Icon.names[routeName]} />
	</Div>
));

const HOCTabIcon = routeName => ({ focused }) => (
	<TabIcon focused={focused} routeName={routeName} />
);

export default HOCTabIcon;
