import { isNil } from 'ramda';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { withTheme } from 'appTheme';
import { BlockTimer } from 'appCommon';
import Text from '../Text';
import Icon from '../Icon';
import Div from '../Div';
import Stl from './style';

const themeTextColor = (transparent, color) => {
	if (transparent) return 'primary';
	if (color) {
		if (color !== 'secondary') return 'secondary';
		return 'textOnWhite';
	} return 'textOnPrimary';
	// TODO -> isDarkTheme
};

class MyButton extends Component {
	componentDidMount() {
		// ? asd
	}

	render() {
		const { icon, iconFamily, transparent, color, upperText, title, onPress, style, theme, ...rest } = this.props;
		return (
			<TouchableOpacity
				onPress={() => BlockTimer.execute(onPress)}
				style={transparent ? Stl.transparent(theme) : Stl.basic(color, theme)}>
				<Div visible={!isNil(icon)} style={Stl.icon}>
					<Icon name={icon} family={iconFamily} color={themeTextColor(transparent, color)} />
				</Div>

				<View style={{ ...Stl.wrapper(theme, icon), ...style }}>
					<Text
						color={themeTextColor(transparent, color)}
						type="medium"
						size="xs"
						{...rest}>
						{(upperText && title) ? title.toUpperCase() : title}
					</Text>
				</View>

			</TouchableOpacity>
		);
	}
}

MyButton.propTypes = {
	style: PropTypes.object,
	upperText: PropTypes.bool,
	title: PropTypes.string.isRequired,
	onPress: PropTypes.func.isRequired,
	theme: PropTypes.object.isRequired,
	transparent: PropTypes.bool,
	color: PropTypes.string,
	icon: PropTypes.string,
	iconFamily: PropTypes.string
};

MyButton.defaultProps = {
	upperText: true,
	transparent: false,
	color: undefined,
	icon: undefined,
	style: {},
	iconFamily: 'FontAwesome'
};

export default withTheme(MyButton);
