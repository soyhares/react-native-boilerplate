import { StyleSheet } from 'react-native';

export default StyleSheet.create(
	{
		basic: (color, theme) => ({
			borderRadius: theme.roundness,
			height: theme.height.button,
			backgroundColor: color ? theme.palette[color] : theme.palette.accent,
			flexDirection: 'row',
			// alignItems: 'center',
			// justifyContent: 'center',
			marginVertical: theme.grid.x2 + theme.grid.h1,
			...theme.boxShadow
		}),
		transparent: theme => ({
			height: theme.height.button,
			marginVertical: theme.grid.x2 + theme.grid.h1,
			flexDirection: 'row',
			width: '100%'
			// alignItems: 'center',
			// justifyContent: 'center'
		}),
		wrapper: (theme, icon) => ({
			// flexDirection: 'row',
			flex: 1,
			alignItems: icon ? undefined : 'center',
			justifyContent: 'center',
			marginLeft: theme.grid[icon ? 'x3' : 'x0']
		}),
		icon: {
			width: 40,
			height: 40,
			alignItems: 'center',
			justifyContent: 'center'
		}
	}
);
