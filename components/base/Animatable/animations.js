/*
    Before to add. Test animations in both platforms.
    Look at: https://github.com/oblador/react-native-animatable
*/

const attention = [
	'bounce',
	'flash',
	'jello',
	'pulse',
	'rotate',
	'rubberBand',
	'shake',
	'swing',
	'tada',
	'wobble'
];

const bouncingIn = [
	'bounceIn',
	'bounceInDown',
	'bounceInUp',
	'bounceInLeft',
	'bounceInRight'
];

const bouncingOut = [
	'bounceOut',
	'bounceOutDown',
	'bounceOutUp',
	'bounceOutLeft',
	'bounceOutRight'
];

const fadingIn = [
	'fadeIn',
	'fadeInDown',
	'fadeInDownBig',
	'fadeInUp',
	'fadeInUpBig',
	'fadeInLeft',
	'fadeInLeftBig',
	'fadeInRight',
	'fadeInRightBig'
];

const fadingOut = [
	'fadeOut',
	'fadeOutDown',
	'fadeOutDownBig',
	'fadeOutUp',
	'fadeOutUpBig',
	'fadeOutLeft',
	'fadeOutLeftBig',
	'fadeOutRight',
	'fadeOutRightBig'
];

const flippers = [
	'flipInX',
	'flipInY',
	'flipOutX',
	'flipOutY'
];

const lightspeed = [
	'lightSpeedIn',
	'lightSpeedOut'
];

const slidingIn = [
	'slideInDown',
	'slideInUp',
	'slideInLeft',
	'slideInRight'
];

const slidingOut = [
	'slideOutDown',
	'slideOutUp',
	'slideOutLeft',
	'slideOutRight'
];

const zoomingIn = [
	'zoomIn',
	'zoomInDown',
	'zoomInUp',
	'zoomInLeft',
	'zoomInRight'
];

const zommingOut = [
	'zoomOut',
	'zoomOutDown',
	'zoomOutUp',
	'zoomOutLeft',
	'zoomOutRight'
];

export default [
    ...attention,
    ...bouncingIn,
    ...bouncingOut,
    ...fadingIn,
    ...fadingOut,
    ...flippers,
    ...lightspeed,
    ...slidingIn,
    ...slidingOut,
    ...zoomingIn,
    ...zommingOut
];
