import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native-animatable';
import Div from '../Div';
import animations from './animations';

const Animatable = ({ children, ref, duration, delay, animation, ...rest }) => (
	<View {...{
		ref,
		delay,
		duration,
		animation
	}}>
		<Div {...rest}>
			{children}
		</Div>
	</View>
);

Animatable.propTypes = { children: PropTypes.element.isRequired };

Animatable.propTypes = {
	ref: PropTypes.func,
	delay: PropTypes.number,
	duration: PropTypes.number,
	animation: PropTypes.oneOf(animations)
};

Animatable.defaultProps = {
	delay: 150,
	ref: undefined,
	duration: 1500,
	animation: 'slideInUp'
};


export default Animatable;
