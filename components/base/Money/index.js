import React from 'react';
import { keys } from 'ramda';
import PropTypes from 'prop-types';
import Currency from 'currency-formatter';
import currencies from 'currency-formatter/currencies.json';
import { Razor } from 'appCommon';
import Text from '../Text';
import Div from '../Div';

const Money = ({ value, code, customFormat, unformat, divProps, ...otherProps }) => {
	let money = Currency[unformat ? 'unformat' : 'format'](value, { code });
	if (Razor.isValidValue(customFormat)) {
		money = Currency.format(value, customFormat);
	}
	return (
		<Div {...divProps}>
			<Text {...otherProps}>{money}</Text>
		</Div>
	);
};

Money.propTypes = {
	unformat: PropTypes.bool,
	divProps: PropTypes.object,
	customFormat: PropTypes.object,
	value: PropTypes.number.isRequired,
	code: PropTypes.oneOf(keys(currencies))
};

Money.defaultProps = {
	code: 'CRC',
	divProps: {},
	customFormat: {},
	unformat: false

};

export default Money;
