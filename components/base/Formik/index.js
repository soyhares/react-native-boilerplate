import { View } from 'react-native';
import { compose } from 'recompose';
import * as Yup from 'yup';
import { Formik as FormikNative } from 'formik';
import {
	handleTextInput,
	withFormikControl,
	withNextInputAutoFocusForm,
	withNextInputAutoFocusInput
} from 'react-native-formik';
import Input from '../Input';
import Switch from '../Item/Switch';
// import setLocale from './setLocale';

const Formik = FormikNative;
// Formik.Yup = (langCode) => ({ setLocale(langCode), ...Yup });
Formik.Switch = withFormikControl(Switch);
Formik.Form = withNextInputAutoFocusForm(View);
Formik.Input = compose(handleTextInput,	withNextInputAutoFocusInput)(Input);

export default Formik;
