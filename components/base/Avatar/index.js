import React from 'react';
import { View, Image } from 'react-native';
import PropTypes from 'prop-types';
import { Razor } from 'appCommon';
import { withTheme } from 'appTheme';
import Text from '../Text';
import Icon from '../Icon';
import Style from './style';

const Avatar = ({ uri, emptyLabel, theme }) => {
	if (Razor.isValidValue(uri) && Razor.isValidURL(uri)) {
		return <Image style={Style.container} source={{ uri }} />;
	}
	return (
		<View style={Style.container(theme)}>
			<Icon color="white" name={Icon.names.camera} />
			<Text color="white" size="xs">{emptyLabel}</Text>
		</View>
	);
};

Avatar.propTypes = {
	uri: PropTypes.string,
	theme: PropTypes.object.isRequired,
	emptyLabel: PropTypes.string.isRequired
};
Avatar.defaultProps = { uri: undefined };

export default withTheme(Avatar);
