import { StyleSheet } from 'react-native'

export default StyleSheet.create(
	{
		container: ({ width, height, palette }) => ({
			alignItems: 'center',
			justifyContent: 'center',
			width: width.avatar,
			height: height.avatar,
			backgroundColor: palette.accent,
			borderRadius: height.avatar / 2
		})
	}
);
