import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StatusBar } from 'react-native';
import { withTheme } from 'appTheme';
import Content from '../Content';
import Header from '../Header';
import SafeAreaView from '../SafeAreaView';
import Stl from './style';

/**
 * @property {string} devName Nombre usado para desarrollo.
 * @property {string} fullScreenn Elimina paddings.
 * @property {boolean} scrollable Si es === false, no se renderiza scrollView
 */
class Page extends Component {
	componentDidMount() {
		const { statusBarColor, barStyle, theme } = this.props;
		StatusBar.setBarStyle(`${barStyle}-content`);
		if (!__IS_IOS__) StatusBar.setBackgroundColor(theme.palette[statusBarColor]);
		StatusBar.setHidden(false);
		if (__DEV__) log('INSTACE OF', this.props.devName);
	}

	renderContentComponent = () => {
		const { scrollable, header, theme, onRefresh, ...rest } = this.props;
		const hasEmptyHeader = !!(header && header.type === 'empty');
		if (!scrollable) return <View style={Stl.f1} {...rest} />;
		return <Content style={Stl.content(hasEmptyHeader, theme)} onRefresh={onRefresh} {...rest} />;
	};

	renderHeader = () => {
		const { header } = this.props;
		if (header) return <Header config={header} />;
		return null;
	};

	render() {
		const { theme, staticBottom, staticTop, color, statusBarColor, fullScreen } = this.props;
		return (
			<SafeAreaView bgColor={statusBarColor}>
				<View style={Stl.wrapper(theme, color, fullScreen)}>
					{this.renderHeader()}
					{staticTop}
					{this.renderContentComponent()}
					{staticBottom}
				</View>
			</SafeAreaView>
		);
	}
}

Page.header = Header.types;

Page.propTypes = {
	color: PropTypes.string,
	header: PropTypes.object,
	onRefresh: PropTypes.func,
	scrollable: PropTypes.bool,
	fullScreen: PropTypes.bool,
	staticTop: PropTypes.element,
	staticBottom: PropTypes.element,
	statusBarColor: PropTypes.string,
	theme: PropTypes.object.isRequired,
	devName: PropTypes.string.isRequired,
	barStyle: PropTypes.oneOf(['light', 'dark'])
};

Page.defaultProps = {
	barStyle: 'light',
	color: 'background',
	header: undefined,
	fullScreen: false,
	onRefresh: () => {},
	staticTop: undefined,
	scrollable: undefined,
	staticBottom: undefined,
	statusBarColor: 'primaryDark'
};

export default withTheme(Page);
