import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	f1: { flex: 1 },
	wrapper: (theme, color, fullScreen) => ({
		backgroundColor: theme.palette[color],
		paddingHorizontal: fullScreen ? 0 : theme.spacing.contentPadHorizontal,
		flex: 1
	}),
	content: (hasEmptyHeader, theme) => ({
		paddingTop: hasEmptyHeader ? theme.height.header + theme.grid.x1 : undefined
	})
});
