import React from 'react';
import PropTypes from 'prop-types';
import { View, Text as TextNative } from 'react-native';
import { Metrics, StyleSheet, Palette, Fonts, Normalize } from 'appTheme';
import Text from '../../base/Text';

const { container, text, messageWrapper } = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginHorizontal: Metrics.Grid.x10,
		height: (Metrics.HEIGHT - ((Metrics.Height.header * 3) + Metrics.Height.bottomTabs))
	},
	text: {
		fontSize: Normalize(100),
		color: Palette.silver,
		fontFamily: Fonts.types.black
	},
	messageWrapper: {
		marginTop: Metrics.Grid.x4
	}
});

const ErrorScreen = ({ errorMessage }) => (
	<View style={container}>
		<TextNative style={text}>D+</TextNative>
		<Text center color="silver" size="xl">E R R O R !</Text>
		<View style={messageWrapper}>
			<Text center color="lightgray">{errorMessage}</Text>
		</View>
	</View>
);

ErrorScreen.propTypes = { errorMessage: PropTypes.string };
ErrorScreen.defaultProps = { errorMessage: '' };

export default ErrorScreen;
