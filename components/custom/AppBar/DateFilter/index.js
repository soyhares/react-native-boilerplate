import M from 'moment';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { Palette, Metrics } from 'appTheme';
import { NavigationService } from 'appNavigation';
import Icon from '../../../base/Icon';
import Text from '../../../base/Text';
import Div from '../../../base/Div';

const Style = StyleSheet.create({
	container: backgroundColor => ({
		backgroundColor,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		height: Metrics.Height.header
	}),
	sorter: { backgroundColor: Palette.black }
});


export default class DateFilter extends Component {
	static propTypes = {
		dark: PropTypes.bool,
		onDateChange: PropTypes.func.isRequired
	};

	static defaultProps = {
		dark: false
	}

	constructor(props) {
		super(props);
		this.dateFormat = 'DD MMM YYYY';
		this.state = { currentDate: M() };
	}

	datesHandler = (isNext) => {
		const { currentDate } = this.state;
		const op = isNext ? 'add' : 'subtract';
		const current = M(currentDate)[op](1, 'd');
		this.setState({ currentDate: current }, () => this.props.onDateChange(current));
	}

	goSortScreen = () => NavigationService.navigate('_sorter');

	render() {
		const { dark } = this.props;
		const { currentDate } = this.state;
		const backgroundColor = Palette[dark ? 'gray' : 'surface'];
		const arrowColor = dark ? 'white' : 'primary';
		const textColor = dark ? 'white' : 'black';
		return (
			<Div row style={{ flex: 1 }}>
				<Div ph="x5" onPress={this.goSortScreen} ai="center" jc="center" style={Style.sorter}>
					<Icon name={Icon.names.sorter} color="white" />
				</Div>
				<View style={Style.container(backgroundColor)}>
					<Div ph="x5" onPress={() => this.datesHandler(false)} ai="center" jc="center">
						<Icon name={Icon.names.before} color={arrowColor} />
					</Div>
					<Div>
						<Text color={textColor}>{currentDate.format(this.dateFormat)}</Text>
					</Div>
					<Div ph="x5" onPress={() => this.datesHandler(true)} ai="center" jc="center">
						<Icon name={Icon.names.next} color={arrowColor} />
					</Div>
				</View>
			</Div>
		);
	}
}
