import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { withTheme, StyleSheet } from 'appTheme';
import { Text, Div } from 'appComponents';

import SearhAppBar from '../Search';

const BAUM = 'B A U M';

const makeStyles = theme => StyleSheet.create({
	headerWrapper: {
		backgroundColor: theme.palette.primary,
		height: theme.height.header,
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'space-between'
	}
});

class AppBarHeader extends React.PureComponent {
	render() {
		const Stl = makeStyles(this.props.theme);
		const { onSearchPress } = this.props;
		return (
			<View style={Stl.headerWrapper}>
				<Div ml="x5">
					<Text size="lg" type="medium" color="white">
						{BAUM}
					</Text>
				</Div>
				<SearhAppBar onSearchPress={onSearchPress} />
			</View>
		);
	}
}

AppBarHeader.propTypes = {
	theme: PropTypes.object.isRequired
};

export default withTheme(AppBarHeader);
