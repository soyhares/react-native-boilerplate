import React from 'react';

const SearchContext = React.createContext(undefined);

export default SearchContext;
