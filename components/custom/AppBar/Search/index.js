import React from 'react';
import { Icon, Div } from 'appComponents';

import SearchContext from '../context';

class SearhAppBar extends React.PureComponent {
	render() {
		return (
			<SearchContext.Consumer>
				{(value) => {
					if (!value) return null;
					return (
						<Div onPress={value} mr="x5">
							<Icon color="white" name={Icon.names.search} />
						</Div>
					);
				}}
			</SearchContext.Consumer>

		);
	}
}

SearhAppBar.propTypes = {
	// onPress: PropTypes.func.isRequired
};

export default SearhAppBar;
