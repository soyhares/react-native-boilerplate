import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { map, equals, replace, takeLast, forEach, dropLast, trim, split, concat } from 'ramda';
import { Razor } from 'appCommon';
import Div from '../../../base/Div';
import Text from '../../../base/Text';
import Divider from '../../../base/Divider';
import List from '../../../base/List';

const Tab = ({ isCurrent, text }) => (
	<Div f={1} mh="x5" jc="flex-end">
		<Div mb="x3">
			<Text type="bold" color="white" size="sm">{text}</Text>
		</Div>
		<Divider fullWidth visible={isCurrent} />
	</Div>
);

Tab.propTypes = {
	isCurrent: PropTypes.bool,
	text: PropTypes.string.isRequired
};

Tab.defaultProps = { isCurrent: false };

export default class TabsFilter extends Component {

	static propTypes = { options: PropTypes.array.isRequired, onChange: PropTypes.func.isRequired };

	activeCondition = ({ item, itemPressed }) => equals(item, itemPressed);

	renderItem = ({ item: { active, label } }) => (<Tab isCurrent={active} text={label} />);

	render = () => (
		<List
			horizontal
			animatedList
			data={this.props.options}
			renderItem={this.renderItem}
			onChange={this.props.onChange}
			activeItemCondition={this.activeCondition} />
	);
}

// TODO: Consultarse por Query
const guest = ['hoy'];
const free = ['hoy', 'punto-del-reporte', 'opinion', 'internacionales', 'asamblea', 'columnas', 'educacion-financiera', 'teclado-abierto', 'podcast',];
const plus = ['reporte-delfino', 'barra-de-prensa', 'lo-personal-es-politico', 'a-fondo', 'civica-2-0', 'sonorama', 'infografico', 'quiero-entender', 'repaso-dominical', 'al-grano',];
// ? Text Formatter
const label = (entry) => {
	let result = entry;
	const oRule = txt => (equals(takeLast(2, trim(txt)), 'on') ? replace('on', 'ón', txt) : txt);
	const rules = [oRule]; // Add more rules
	const words = split('-', result);
	forEach((rule) => {
		let sentence = '';
		forEach((word) => { sentence = concat(sentence, `${rule(word)} `); }, words);
		result = dropLast(1, sentence);
	}, rules);
	return Razor.capEntry(result);
};

TabsFilter.CategoriesGuest = map(category => ({ label: label(category), category }), guest);
TabsFilter.CategoriesFree = map(category => ({ label: label(category), category }), free);
TabsFilter.CategoriesPlus = map(category => ({ label: label(category), category }), plus);
