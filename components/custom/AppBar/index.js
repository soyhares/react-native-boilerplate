import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { withTheme, StyleSheet } from 'appTheme';
import SearchContext from './context';
import AppBarHeader from './Header';
import AppBarDateFilter from './DateFilter';
import AppBarTabsFilter from './TabsFilter';

const makeStyles = theme => StyleSheet.create({
	actionsWrapper: {
		backgroundColor: theme.palette.primary,
		height: theme.height.header,
		flexDirection: 'row'
	}
});

class AppBar extends PureComponent {
	render() {
		const { children, theme, onSearchPress } = this.props;
		const Stl = makeStyles(theme);
		return (
			<SearchContext.Provider value={onSearchPress}>
				<AppBarHeader />
				{children ? (
					<View style={Stl.actionsWrapper}>
						{children}
					</View>
				) : null
				}
			</SearchContext.Provider>
		);
	}
}

AppBar.DateFilter = AppBarDateFilter;
AppBar.TabsFilter = AppBarTabsFilter;

AppBar.propTypes = {
	theme: PropTypes.object.isRequired,
	onSearchPress: PropTypes.func,
	children: PropTypes.node
};

AppBar.defaultProps = {
	children: undefined,
	onSearchPress: undefined
};

export default withTheme(AppBar);
