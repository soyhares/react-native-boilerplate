import React from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'appTheme';
import Text from '../../base/Text';
import Div from '../../base/Div';
import Divider from '../../base/Divider';

const Logo = ({ text, ...rest }) => (
	<Text size="lg" type="bold" {...rest}>
		{` ${text}`}
		<Text color="primary" size="lg" type="bold">+</Text>
	</Text>
);

Logo.propTypes = { text: PropTypes.string.isRequired };

const Title = ({ theme, underline, addLogo, logo, mv, pv, ph, mh, children, ...rest }) => (
	<Div {...{ mv, pv, ph, mh }}>
		<Text type="bold" {...rest}>
			{children}
			{addLogo ? <Logo text={logo} {...rest} /> : null}
		</Text>
		<Divider mv={mv} visible={underline} />
	</Div>
);

Title.propTypes = {
	size: PropTypes.string,
	color: PropTypes.string,
	mv: PropTypes.string,
	mh: PropTypes.string,
	addLogo: PropTypes.bool,
	children: PropTypes.node,
	underline: PropTypes.bool,
	theme: PropTypes.object.isRequired,
	logo: PropTypes.oneOf(['D', 'Delfino'])
};
Title.defaultProps = {
	size: 'lg',
	color: 'black',
	mv: 'x2',
	mh: 'x5',
	logo: 'D',
	addLogo: false,
	underline: true,
	children: undefined
};

export default withTheme(Title);
