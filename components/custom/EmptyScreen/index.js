import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text as TextNative } from 'react-native';
import { Metrics, StyleSheet, Palette, Fonts, Normalize } from 'appTheme';
import Text from '../../base/Text';

const { container, text } = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginHorizontal: Metrics.Grid.x10,
		height: (Metrics.HEIGHT - ((Metrics.Height.header * 3) + Metrics.Height.bottomTabs))
	},
	text: {
		color: Palette.silver,
		fontSize: Normalize(100),
		fontFamily: Fonts.types.black
	}
});

const lang = {
	es: 'S I N  R E S U L T A D O S',
	en: 'N O T  R E S U L T S'
};

const EmptyScreen = ({ code }) => (
	<View style={container}>
		<TextNative style={text}>D+</TextNative>
		<Text center color="silver" size="xl">{lang[code]}</Text>
	</View>
);

EmptyScreen.propTypes = { code: PropTypes.string.isRequired };

const mstp = ({ settings: { languageCode: code } }) => ({ code });

export default connect(mstp)(EmptyScreen);
